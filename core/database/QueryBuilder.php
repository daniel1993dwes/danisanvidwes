<?php

namespace dwes\core\database;

use dwes\core\App;
use dwes\app\entity\IEntity;
use PDO;

abstract class QueryBuilder
{
    private $pdo;
    private $tableName;
    private $entityName;

    public function __construct(
        string $tableName, string $entityName)
    {
        $this->pdo = App::getConnection();
        $this->tableName = $tableName;
        $this->entityName = $entityName;
    }

    public function find(int $id)
    {
        $sql = "SELECT * FROM " . $this->tableName . " WHERE id=$id";
        $pdoStatement = $this->pdo->prepare($sql);

        $pdoStatement->execute();

        $pdoStatement->setFetchMode(PDO::FETCH_CLASS, $this->entityName);
        return $pdoStatement->fetch();
    }

    private function getStrParams(array $parametros, string $conector)
    {
        $fields = [];
        $keys = array_keys($parametros);

        foreach ($keys as $key)
        {
            $fields[] = $key . ' = :' . $key;
        }

        return implode($conector, $fields);
    }

    public function findBy(array $parametros)
    {
        $fields = $this->getStrParams($parametros, ' and ');

        $sql = sprintf(
            "SELECT * FROM %s WHERE %s",
            $this->tableName,
            $fields
        );

        $pdoStatement = $this->pdo->prepare($sql);

        $pdoStatement->execute($parametros);

        return $pdoStatement->fetchAll(
            PDO::FETCH_CLASS, $this->entityName);
    }

    public function findAll() : array
    {
        $sql = "SELECT * FROM " . $this->tableName;
        $pdoStatement = $this->pdo->prepare($sql);

        $pdoStatement->execute();

        return $pdoStatement->fetchAll(
            PDO::FETCH_CLASS, $this->entityName);
    }

    public function executeSql(string $sql, array $parametros=[])
    {
        $pdoStatement = $this->pdo->prepare($sql);
        $pdoStatement->execute($parametros);

        return $pdoStatement->fetchAll(
            PDO::FETCH_CLASS, $this->entityName
        );
    }

    public function executeSqlNotReturn(string $sql, array $parametros=[])
    {
        $pdoStatement = $this->pdo->prepare($sql);
        $pdoStatement->execute($parametros);
    }

    public function executeSqlGeneric(string $sql, array $parametros=[])
    {
        $pdoStatement = $this->pdo->prepare($sql);
        $pdoStatement->execute($parametros);

        return $pdoStatement->fetchAll();
    }

    public function insert(IEntity $entity)
    {
        $parametros = $entity->toArray();
        $fieldsName = implode(', ', array_keys($parametros));
        $fieldsValue = ':' . implode(', :', array_keys($parametros));

        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s)",
            $this->tableName,
            $fieldsName,
            $fieldsValue
        );
        $pdoStatement = $this->pdo->prepare($sql);
        $pdoStatement->execute($parametros);
    }

    public function delete(int $id)
    {
        $sql = sprintf(
            "DELETE FROM %s WHERE id=$id",
            $this->tableName
        );
        $pdoStatement = $this->pdo->prepare($sql);
        $pdoStatement->execute();
    }

    public function update(IEntity $entity)
    {
        $parametros = $entity->toArray();

        $fields = $this->getStrParams($parametros, ', ');

//        Estructura a obtener
//        ['nombre' => 'alex', 'apellido' => 'amat']
//        UPDATE SET nombre = :nombre, apellido = :apellido;

        $sql = sprintf(
            "UPDATE %s SET %s WHERE id=" . $entity->getId(),
            $this->tableName,
            $fields
        );

        $pdoStatement = $this->pdo->prepare($sql);
        $pdoStatement->execute($parametros);
    }
}