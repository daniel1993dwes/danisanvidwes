<?php

namespace dwes\core\database;

use dwes\core\App;
use Exception;
use PDO;
use PDOException;

class Connection
{
    /**
     * @return PDO
     * @throws Exception
     */
    public static function make()
    {
        try
        {
            $dbConfig = App::get('config')['database'];

            $pdo = new PDO(
                $dbConfig['connection'] . ';dbname=' . $dbConfig['name'],
                $dbConfig['username'],
                $dbConfig['password'],
                $dbConfig['options']);

            return $pdo;
        }
        catch (PDOException $exception)
        {
            die($exception->getMessage());
        }
    }
}