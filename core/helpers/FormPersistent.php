<?php

namespace dwes\core\helpers;


class FormPersistent
{
    public static function get(
        string $key, $default = '') : string
    {
        $value = $default;

        if (isset($_SESSION['form']))
        {
            $value = $_SESSION['form'][$key] ?? $default;

            $_SESSION['form'][$key] = NULL;

            unset($_SESSION['form'][$key]);
        }

        return $value;
    }

    public static function set(string $key, $value)
    {
        $_SESSION['form'][$key] = $value;
    }

    public static function unsetMessage(string $key)
    {
        if (isset($_SESSION['form']))
            unset($_SESSION['form'][$key]);
    }
}