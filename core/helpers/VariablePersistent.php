<?php

namespace dwes\core\helpers;


class VariablePersistent
{
    public static function get(
        string $key, $default = null)
    {
        $value = $default;

        if (isset($_SESSION[$key]))
        {
            $value = $_SESSION[$key] ?? $default;

            $_SESSION[$key] = NULL;

            unset($_SESSION[$key]);
        }

        return $value;
    }

    public static function set(string $key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function unsetMessage(string $key)
    {
        if (isset($_SESSION[$key]))
            unset($_SESSION[$key]);
    }
}