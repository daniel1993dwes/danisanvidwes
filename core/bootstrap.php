<?php

use dwes\core\App;

session_start();

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../app/config.php';
App::bind('config', $config);

if (isset($_SESSION['idioma']))
    $language = $_SESSION['idioma'];
else
{
    $language = 'es_ES';
    $_SESSION['idioma'] = $language;
}

$language .= ".utf8";

setlocale(LC_ALL, $language);

bindtextdomain("messages", "../locale");

bind_textdomain_codeset("messages", "UTF-8");

textdomain("messages");
