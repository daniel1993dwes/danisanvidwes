<?php

namespace dwes\core;


class Response
{
    public static function renderView($name, $data = [])
    {
        extract($data);

        $app['user'] = App::get('user');

        ob_start();

        require __DIR__ . "/../app/views/$name.view.php";

        $mainContent = ob_get_clean();

        require __DIR__ . '/../app/views/layout.view.php';
    }
}