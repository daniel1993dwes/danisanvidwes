<?php

namespace dwes\core;

use dwes\core\database\Connection;
use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class App
{
    private static $container = [];

    public static function bind(string $key, $value)
    {
        static::$container[$key] = $value;
    }

    public static function get(string $key)
    {
        if (! array_key_exists($key, static::$container))
            throw new Exception("No se ha encontrado la clave $key en el contenedor");

        return static::$container[$key];
    }

    public static function getConnection()
    {
        if (! array_key_exists('connection', static::$container))
            static::$container['connection'] = Connection::make();

        return static::$container['connection'];
    }

    public static function getRepository(string $className)
    {
        if (! array_key_exists($className, static::$container))
            static::$container[$className] = new $className();

        return static::$container[$className];
    }

    public static function getLogger()
    {
        $config = App::get('config');

        if (! array_key_exists('logger', static::$container))
        {
            static::$container['logger'] = new Logger(
                $config['logger']['name']);

            static::$container['logger']->pushHandler(
                new StreamHandler(
                    $config['logger']['file'],
                    $config['logger']['level'])
            );
        }

        return static::$container['logger'];
    }

    public static function CreateEmail()
    {
        $config = App::get('config');
        // Create the Transport
        $transport = (new Swift_SmtpTransport($config['mailer']['smtp'], $config['mailer']['port']))
            ->setUsername($config['mailer']['correo'])
            ->setPassword($config['mailer']['password'])
            ->setEncryption($config['mailer']['encrypt']);
        
        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);
    }

    public static function EnviarMensaje($mailer, string $title, array $to, string $body)
    {
        $config = App::get('config');
        // Create a message
        $message = (new Swift_Message($title))
            ->setFrom($config['mailer']['correo'])
            ->setTo($to)
            ->setBody($body)
        ;

        // Send the message
        $result = $mailer->send($message);
    }
}