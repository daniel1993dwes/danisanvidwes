<?php

use dwes\app\utils\Utils;

?>
<div class="clear"></div>
<div id="page-content">
    <section class="breadcrumb">
        <div class="container">
            <h2>My Profile</h2>
        </div>
    </section>
    <section class="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <!-- POST SUSTITUIR -->
                    <div class="single-post">
                        <div class="blog-img venta">
                            <img src="<?= $user->getUrlAvatar() ?>" style="height: 150px">
                        </div>
                        <h2 class="color-texto"><?= $user->getName() ?></h2>
                        <h3 class="color-texto"><?= $user->getApellidos() ?></h3>
                        <h4 class="color-texto"><?= $user->getEmail() ?></h4>
                        <div class="blog-meta">
                            Inscrito desde: <i
                                        class="blue-text fa fa-calendar"></i> <?= Utils::sacarFechaCorta($user->getFecha()) ?>
                        </div>
                        <h2>Inscriptions</h2>
                        <?php if (count($events) > 0) : ?>
                            <div class="display-line sm-display">
                                <div class="container">
                                    <?php foreach ($events as $event) : ?>
                                        <a href="/details/<?= $event->getIDEVENTO() ?>">
                                            <h5><?= $event->getPost($event->getIDEVENTO())->getTitulo() ?></h5></a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php else : ?>
                            <h3>Youd dont have inscriptions actives</h3>
                        <?php endif; ?>
                    </div>
                </div>
                <section class="col-sm-5 col-md-4 col-lg-4">
                    <?php if (isset($error) && !empty($error)) : ?>
                        <div class="row">
                            <div class="alert alert-danger" role="alert">
                                <?= $error ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (isset($nuevo) && !empty($nuevo)) : ?>
                        <div class="row">
                            <div class="alert alert-success" role="alert">
                                <?= $nuevo ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div>
                        <h2>Language for your profile</h2>
                        <div class="form-group">
                            <label for="password">Choose your language for your profile</label>
                            <div class="row espacio-pequeño">
                                <a href="/idioma/1" name="language"> <img alt="Bandera de España" src="//www.banderas-mundo.es/data/flags/mini/es.png" width="40" height="20" /> ESPAÑOL</a>
                            </div>
                            <div class="row espacio-pequeño">
                                <a href="/idioma/2" name="language"> <img alt="Bandera de Reino Unido" src="//www.banderas-mundo.es/data/flags/mini/gb.png" width="40" height="20" /> INGLES</a>
                            </div>
                        </div>
                        <h2>Change password</h2>
                        <form
                        action="/change-password"
                        method="post"
                        enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="password">Introduce new password</label>
                                <input type="text" name="pas1" placeholder="Password">
                                <label for="password">Introduce other way new password</label>
                                <input type="text" name="pas2" placeholder="Password">
                                <input type="text" name="id" class="hidden" value="<?= $user->getId() ?>">
                            </div>
                            <div class="text-center">
                                <input type="submit" class="btn-default" value="Change Password">
                            </div>
                        </form>
                        <h2>Change Avatar Image</h2>
                        <form
                        action="/change-avatar"
                        method="post"
                        enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="password">Upload new file Image</label>
                                <input type="file" name="image" placeholder="Image for upload"/>
                                <input type="text" name="id" class="hidden" value="<?= $user->getId() ?>">
                            </div>
                            <div class="text-center">
                                <input type="submit" class="btn-default" value="Change Avatar">
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</div>
<div class="clear"></div>
