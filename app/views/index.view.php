<?php

use dwes\app\utils\Utils;
use dwes\app\entity\Post;
use dwes\app\entity\Usuario;

?>
<div class="clear"></div>
<div id="page-content">
    <section class="flexslider index">
    </section>
    <div class="container">
        <div class="row">
            <section class="col-sm-7 col-md-8 col-lg-8">
                <div class="intro">
                    <h2>Welcome to the ACME ASOCIATION</h2>
                    <p>If you love the modern culture of the world and the games, join us.</p>
                    <ul class="row">
                        <li class="col-sm-4">
                            <i class="fas fa-gamepad"></i>
                            <h3>Games and more</h3>
                            <p>Join to the glory of the gamer.</p>
                        </li>
                        <li class="col-sm-4">
                            <i class="far fa-comment-dots"></i>
                            <h3>Any questions? FAQs</h3>
                            <p>Visit page with own frequent ask and questions.</p>
                        </li>
                        <li class="col-sm-4">
                            <i class="far fa-paper-plane"></i>
                            <h3>Contact if you prefer</h3>
                            <p>Contact is ever the best option.</p>
                        </li>
                    </ul>
                    <h1 class="venta">Join to the Asocition ACME now!!</h1>
                    <p id="venta">Win a lot of prizes or participate in days of video games, board games, role playing
                        and much more.
                        If you are one of those who love good vibes, video game afternoons, anime or you are interested
                        in television series, this is your place.</p>
                </div>
            </section>
            <!-- Search -->
            <section class="col-sm-5 col-md-4 col-lg-4">
                <div class="get-quote-form">
                    <h2>Sign up</h2>
                    <form id="get-quote">
                        <div>
                            <input type="text" name="email" placeholder="Email"/>
                        </div>
                        <div class="form-select">
                            <span></span>
                            <select name="tipo" class="mr-sm-3">
                                <option value="" selected>TYPE OF EVENT</option>
                                <option value="EVENTO GENERICO">GENERIC EVENT</option>
                                <option value="EVENTO COSPLAY">COSPLAY EVENT</option>
                                <option value="EVENTO VIDEOJUEGOS">VIDEOGAMES EVENT</option>
                                <option value="EVENTO DE ROL EN VIVO">EVENT OF ROLE PLAY</option>
                                <option value="EVENTO PROYECCIÓN DE PELÍCULA">EVENT OF SHOW FILM</option>
                            </select>
                        </div>
                        <div class="text-center">
                            <input type="submit" class="btn-default" value="Get Free Quote"/>
                        </div>
                    </form>
                </div>
            </section>
            <!-- Fin Search -->
        </div>
    </div>
    <section class="blog-page">
        <div class="container">
            <h1 class="venta">Bulletin board!</h1>
            <nav class="navbar navbar-light bg-light">
                <div class="col-sm-8">
                    <form class="form-group"
                          action="/element/filter"
                          method="post"
                          enctype="multipart/form-data">
                        <h3>Aply filters for better results: </h3>
                        <p>Set both dates to search between...</p>
                        <div class="row">
                            <div class="col-sm-5">
                                <label for="fecha1">Find Later Dates: </label>
                                <input class="form-control mr-sm-1" type="date" aria-label="Search" name="fecha1">
                            </div>
                            <div class="col-sm-5">
                                <label for="fecha2">Find Previous Dates: </label>
                                <input class="form-control mr-sm-1" type="date" aria-label="Search" name="fecha2">
                            </div>
                        </div>
                        <label for="tipo">Type of Event:</label>
                        <select name="tipo" class="mr-sm-3">
                            <option value="" selected>TYPE OF EVENT</option>
                            <option value="EVENTO GENERICO">GENERIC EVENT</option>
                            <option value="EVENTO COSPLAY">COSPLAY EVENT</option>
                            <option value="EVENTO VIDEOJUEGOS">VIDEOGAMES EVENT</option>
                            <option value="EVENTO DE ROL EN VIVO">EVENT OF ROLE PLAY</option>
                            <option value="EVENTO PROYECCIÓN DE PELÍCULA">EVENT OF SHOW FILM</option>
                        </select>
                        <div class="form-group">
                            <label for="content">Content on the title: </label>
                            <input class="form-control mr-sm-1" type="text" aria-label="Search" name="texto">
                        </div>
                        <a class="btn btn-secondary" href="/" role="button" id="DropFilter">Drop Filter</a>
                        <button class="btn btn-danger" type="submit">Apply Filter</button>
                    </form>
                    <?php if (isset($filter_aply) && !empty($filter_aply)) : ?>
                        <div class="row">
                            <div class="alert alert-success" role="alert">
                                <?= $filter_aply ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </nav>
            <div class="row">
                <div class="col-sm-8">
                    <h4>There are <?= count($eventos) ?> results found!!</h4>
                    <?php if (empty($eventos)) {
                        echo "<div class='single-post'><h4>Elements not found</h4>
                            <img src='/images/gallery/NotFound.jpg'></div>";
                    } ?>
                    <?php foreach ($eventos as $evento) : ?>
                    <?php if ($evento->isVISIBLE()) : ?>
                        <div class="single-post">
                            <div class="blog-img">
                                <a href="/details/<?= $evento->getId() ?>">
                                    <img src="<?= $evento->getUrlImage() ?>" class="img-responsive">
                                </a>
                            </div>
                            <h2 class="blog-title"><?= $evento->getTitulo() ?></h2></a>
                            <a href="" class="ml-0"><i
                                        class="blue-text fa fa-calendar"></i> <?= Utils::sacarFechaCorta($evento->getFecha()) ?>
                            </a>
                            <a href=""><i class="blue-text fa fa-user"></i>
                                By <?= Post::getUsuario($evento->getIdUsuario())->getName() ?> </a>
                            <a href=""><i class="blue-text fa fa-comment"></i> <?= $evento->getCommentCant() ?> </a>
                            <p><?= htmlspecialchars_decode(Utils::sacarResumen($evento->getTexto())) ?></p>
                            <div class="blog-btn">
                                <a href="/details/<?= $evento->getId() ?>" class="btn-default">Read More</a>
                                <div class="img-inline"><a href="/profile/<?= $evento->getIdUsuario() ?>"><img
                                                src="<?= Post::getUsuario($evento->getIdUsuario())->getUrlAvatar() ?>"><?= Post::getUsuario($evento->getIdUsuario())->getName() ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <!-- BORRAR ESTE BLOG -->
                    <div class="text-center">
                        <ul class="pagination ins-page">
                            <?php if ($TOTAL_PAGINAS > 1) {
                                $pag = intval($pagina);
                                if ($pag != 1) {
                                    echo "<li><a href='/" . ($pag - 1) . "'>Previous</a></li>";
                                }
                                for ($i = 1; $i <= $TOTAL_PAGINAS; $i++) {
                                    if ($pag == $i) {
                                        //si muestro el índice de la página actual, no coloco enlace
                                        echo "<li class='active'><a>$pag</a></li>";
                                    } else {
                                        //si el índice no corresponde con la página mostrada actualmente,
                                        //coloco el enlace para ir a esa página
                                        echo "<li><a href='/$i'>$i</a></li>";
                                    }

                                }
                                if ($pag != $TOTAL_PAGINAS) {
                                    echo "<li><a href='/" . ($pag + 1) . "'><span>Next</span></a></li>";
                                }
                            } ?>
                            <!-- <li class="active"><span>Previous</span></li>

                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">Next</a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="clear"></div>
