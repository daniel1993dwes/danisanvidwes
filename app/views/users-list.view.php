<div class="clear"></div>
<div id="page-content">
    <section class="breadcrumb">
        <div class="container">
            <h2>Users List</h2>
        </div>
    </section>
    <section class="blog-page">
        <div class="container">
            <nav class="navbar navbar-light bg-light">
                <div class="col-sm-8">
                    <?php if (isset($error) && !empty($error)) : ?>
                        <div class="row">
                            <div class="alert alert-danger" role="alert">
                                <?= $error ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (isset($filter_user) && !empty($filter_user)) : ?>
                        <div class="row">
                            <div class="alert alert-success" role="alert">
                                <?= $filter_user ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <form class="form-group"
                          action="/filter-user"
                          method="post"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-5">
                                <label for="fecha1">Find User Name: </label>
                                <input class="form-control mr-sm-1" type="text" aria-label="Search" name="nameUser">
                            </div>
                        </div>
                        <label for="tipo">Type of Role:</label>
                        <select name="tipo" class="mr-sm-3">
                            <option value="" selected>TYPE OF ROLE</option>
                            <option value="ROLE_ADMIN">ROLE ADMIN</option>
                            <option value="ROLE_USER">ROLE USER</option>
                            <option value="ROLE_ANONIMO">ROLE ANONIMO</option>
                        </select>
                        <a class="btn btn-secondary" href="/users-list" role="button" id="DropFilter">Drop Filter</a>
                        <button class="btn btn-danger" type="submit">Apply Filter</button>
                    </form>
                </div>
            </nav>
        </div>
    </section>
</div>
<?php if (count($users) > 0) : ?>
    <table class="table">
        <thead></thead>
        <tr>
            <th scope="col">
                ID
            </th>
            <th scope="col">
                IMAGE
            </th>
            <th scope="col">
                RUTA AVATAR
            </th>
            <th scope="col">
                NOMBRE
            </th>
            <th scope="col">
                APELLIDOS
            </th>
            <th scope="col">
                PROVINCIA
            </th>
            <th scope="col">
                EMAIL
            </th>
            <th scope="col">
                CODIGO
            </th>
            <th scope="col">
                PASSWORD
            </th>
            <th scope="col">
                AVATAR
            </th>
            <th scope="col">
                ROLE
            </th>
            <th scope="col">
                FECHA
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user) : ?>
            <tr scope="row">
                <td class="color-texto">
                    <?= $user->getId() ?>
                </td>
                <td class="color-texto">
                    <img src="<?= $user->getUrlAvatar() ?>" height="100px">
                </td>
                <td class="color-texto">
                    <?= $user->getUrlAvatar() ?>
                </td>
                <td class="color-texto">
                    <?= $user->getName() ?>
                </td>
                <td class="color-texto">
                    <?= $user->getApellidos() ?>
                </td>
                <td class="color-texto">
                    <?= $user->getProvincia() ?>
                </td>
                <td class="color-texto">
                    <?= $user->getEmail() ?>
                </td>
                <td class="color-texto">
                    <?= $user->getCodigo() ?>
                </td>
                <td class="color-texto">
                    <?= $user->getPassword() ?>
                </td>
                <td class="color-texto">
                    <?= $user->getAvatar() ?>
                </td>
                <td class="color-texto">
                    <?= $user->getRole() ?>
                </td>
                <td>
                    <a href="/delete/user/<?= $user->getId() ?>" class="btn btn-primary">Delete User</a>
                </td>
                <td>
                    <form action="/change-role"
                          method="post"
                          enctype="multipart/form-data">
                        <label for="tipo">Type of Role:</label>
                        <select name="tipo" class="mr-sm-3">
                            <option value="" selected>TYPE OF ROLE</option>
                            <option value="ROLE_ADMIN">ROLE ADMIN</option>
                            <option value="ROLE_USER">ROLE USER</option>
                            <option value="ROLE_ANONIMO">ROLE ANONIMO</option>
                        </select>
                        <input type="text" class="hidden" name="idUser" value="<?= $user->getId() ?>">
                        <button class="btn btn-danger" type="submit">Change Role</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
<div class="clear"></div>
