<?php
use dwes\app\utils\Utils;
?>

<div class="clear"></div>
<div id="page-content">
    <section class="blog-page">
        <div class="container">
            <nav class="navbar navbar-light bg-light">
                <div class="col-sm-8">
                    <form class="form-group"
                          action="/filter-elements"
                          method="post"
                          enctype="multipart/form-data">
                        <h3>Aply filters for better results: </h3>
                        <p>Set both dates to search between...</p>
                        <div class="row">
                            <div class="col-sm-5">
                                <label for="fecha1">Find Later Dates: </label>
                                <input class="form-control mr-sm-1" type="date" aria-label="Search" name="fecha1">
                            </div>
                            <div class="col-sm-5">
                                <label for="fecha2">Find Previous Dates: </label>
                                <input class="form-control mr-sm-1" type="date" aria-label="Search" name="fecha2">
                            </div>
                        </div>
                        <label for="tipo">Type of Event:</label>
                        <select name="tipo" class="mr-sm-3">
                            <option value="" selected>TYPE OF EVENT</option>
                            <option value="EVENTO GENERICO">GENERIC EVENT</option>
                            <option value="EVENTO COSPLAY">COSPLAY EVENT</option>
                            <option value="EVENTO VIDEOJUEGOS">VIDEOGAMES EVENT</option>
                            <option value="EVENTO DE ROL EN VIVO">EVENT OF ROLE PLAY</option>
                            <option value="EVENTO PROYECCIÓN DE PELÍCULA">EVENT OF SHOW FILM</option>
                        </select>
                        <div class="form-group">
                            <label for="content">Content on the title: </label>
                            <input class="form-control mr-sm-1" type="text" aria-label="Search" name="texto">
                        </div>
                        <a class="btn btn-secondary" href="/" role="button" id="DropFilter">Drop Filter</a>
                        <button class="btn btn-danger" type="submit">Apply Filter</button>
                    </form>
                    <?php if (isset($filter) && !empty($filter)) : ?>
                        <div class="row">
                            <div class="alert alert-success" role="alert">
                                <?= $filter ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (isset($error) && !empty($error)) : ?>
                        <div class="row">
                            <div class="alert alert-danger" role="alert">
                                <?= $error ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </nav>
        </div>
    </section>
</div>
<div id="page-content">
        <div class="container">
            <h4>There are <?= count($elements) ?> results found!!</h4>
            <?php if (empty($elements)) {
                echo "<div class='single-post'><h4>Elements not found</h4>
                                        <img src='/images/gallery/NotFound.jpg'></div>";
            } ?>
        </div>
</div>
            <?php if (count($elements) > 0) : ?>
            <table class="table">
                <thead></thead>
                <tr>
                    <th scope="col">
                        ID
                    </th>
                    <th scope="col">
                        REAL IMAGE
                    </th>
                    <th scope="col">
                        ID USUARIO
                    </th>
                    <th scope="col">
                        QUANTITY COMMENT
                    </th>
                    <th scope="col">
                        TITLE
                    </th>
                    <th scope="col">
                        IMAGE
                    </th>
                    <th scope="col">
                        TEXT
                    </th>
                    <th scope="col">
                        TYPE
                    </th>
                    <th scope="col">
                        GAUGING
                    </th>
                    <th scope="col">
                        DATE
                    </th>
                    <th scope="col">
                        DATE CREATED
                    </th>
                    <th scope="col">
                        VISIBLE
                    </th>
                    <th scope="col">
                        VISULIZE
                    </th>
                    <th scope="col">
                        EDIT
                    </th>
                    <th scope="col">
                        DELETE
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($elements as $element) : ?>
                    <tr scope="row">
                        <td class="color-texto">
                            <?= $element->getId() ?>
                        </td>
                        <td class="color-texto">
                            <img src="<?= $element->getUrlImage() ?>" style="height: 30px">
                        </td>
                        <td class="color-texto">
                            <?= $element->getIdUsuario() ?>
                        </td>
                        <td class="color-texto">
                            <?= $element->getCommentCant() ?>
                        </td>
                        <td class="color-texto">
                            <?= $element->getTitulo() ?>
                        </td>
                        <td class="color-texto">
                            <?= $element->getImagen() ?>
                        </td>
                        <td class="color-texto">
                            <?= Utils::sacarResumenPequeño($element->getTexto()) ?>
                        </td>
                        <td class="color-texto">
                            <?= $element->getTipo() ?>
                        </td>
                        <td class="color-texto">
                            <?= $element->getAFORO() ?>
                        </td>
                        <td class="color-texto">
                            <?= $element->getFecha() ?>
                        </td>
                        <td class="color-texto">
                            <?= $element->getFechaCreacion() ?>
                        </td>
                        <td class="color-texto">
                            <?= $element->isVisible()? 'YES':'NO'; ?>
                        </td>
                        <td>
                            <a href="/details/<?= $element->getId() ?>" class="btn btn-primary">Visualize</a>
                        </td>
                        <td>
                            <a href="/modify-element/<?= $element->getId() ?>" class="btn btn-primary">Modify Event</a>
                        </td>
                        <td>
                            <a href="/delete-element/<?= $element->getId() ?>" class="btn btn-primary">Delete Event</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
<?php endif; ?>
