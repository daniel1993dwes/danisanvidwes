<?php

use dwes\app\entity\Post;
use dwes\app\utils\Utils;
use dwes\app\entity\Comment;

?>
<div class="clear"></div>
<div id="page-content">
    <section class="breadcrumb">
        <div class="container">
            <h2>Blog Details</h2>
        </div>
    </section>
    <section class="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">

                    <!-- POST SUSTITUIR -->
                    <div class="single-post">
                        <h1 class="venta"><?= $evento->getTitulo() ?></h1>
                        <div class="blog-img venta">
                            <a href="">
                                <img src="<?= $evento->getUrlImage() ?>" class="img-responsive">
                            </a>
                        </div>
                        <?php if ($app['user'] == null) :?>
                            <div class="row espacio">
                                <div class="alert alert-danger" role="alert">
                                    You must be registered to get inscription and send message to the autor
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="blog-meta">
                            <a href="" class="ml-0"><i
                                        class="blue-text fa fa-calendar"></i> <?= Utils::sacarFechaCorta($evento->getFechaCreacion()) ?>
                            </a>
                            <a href="/user/events/<?= $evento->getIdUsuario() ?>"><i class="blue-text fa fa-user"></i>
                                By <?= Post::getUsuario($evento->getIdUsuario())->getName() ?></a>
                            <a href=""><i class="blue-text fa fa-comment"></i> (<?= $evento->getCommentCant() ?>)
                                Comments </a>
                            <?php if (!is_null($app['user'])) : ?>
                                <a href="/sent/<?= Post::getUsuario($evento->getIdUsuario())->getId() ?>"
                                   class="btn btn-danger espacio-pequeño" style="color: white" id="delete"
                                   role="button">Sent Message</a>
                                <a href="/get-inscripcion/<?= $evento->getId() ?>" class="btn btn-primary" style="color: white" role="button">Inscribirse</a>
                            <?php endif; ?>
                        </div>
                        <div class="text-justify">
                            <?= htmlspecialchars_decode($evento->getTexto()) ?>
                        </div>

                    </div>
                    <!-- Related Post -->
                    <h2>Related Post</h2>
                    <div class="relate-post">
                        <div class="display-line sm-display">
                            <div class="container">
                                <?php foreach ($relacionados as $relacionado) :
                                    if ($relacionado->getId() !== $evento->getId()) { ?>
                                        <a href="/details/<?= $relacionado->getId() ?>">
                                            <h6><?= $relacionado->getTitulo() ?></h6></a>
                                    <?php }
                                endforeach; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php if (isset($primero) && !empty($primero)) : ?>
                                    <div class="row">
                                        <div class="alert alert-success" role="alert">
                                            <?= $primero ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($ultimo) && !empty($ultimo)) : ?>
                                    <div class="row">
                                        <div class="alert alert-success" role="alert">
                                            <?= $ultimo ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="mt-30 display-line">
                            <div>
                                <form action="/previous"
                                      method="post"
                                      enctype="multipart/form-data">
                                    <input type="text" value="<?= $evento->getFechaCreacion() ?>" class="hidden"
                                           name="fecha">
                                    <button type="submit" class="blue-text"><i
                                                class="glyphicon glyphicon-circle-arrow-left"></i><span class="ml-5">Previous Post</span>
                                    </button>
                                </form>
                            </div>
                            <div class="ml-auto">
                                <div class="text-right">
                                    <form action="/next"
                                          method="post"
                                          enctype="multipart/form-data">
                                        <input type="text" value="<?= $evento->getFechaCreacion() ?>" class="hidden"
                                               name="fecha">
                                        <button type="submit" class="blue-text"><span class="mr-5">Next Post</span><i
                                                    class="glyphicon glyphicon-circle-arrow-right"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Leave comment -->
                    <?php if (!is_null($app['user'])) : ?>
                        <h2>Leave A Comments</h2>
                        <?php if (isset($nuevo_comentario) && !empty($nuevo_comentario)) : ?>
                            <div class="row espacio">
                                <div class="alert alert-success" role="alert">
                                    <?= $nuevo_comentario ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (isset($error_comentario) && !empty($error_comentario)) : ?>
                            <div class="row espacio">
                                <div class="alert alert-danger" role="alert">
                                    <?= $error_comentario ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <form class="comment"
                              action="/comments"
                              method="post"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div>
                                        <input type="text" name="idUser" class="hidden"
                                               value="<?= $app['user']->getId() ?>"/>
                                    </div>
                                    <div>
                                        <input type="text" name="idPost" class="hidden"
                                               value="<?= $evento->getId() ?>"/>
                                        <input type="text" name="idComment" class="hidden" value="-1"/>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div>
                                        <textarea rows="1" cols="1" name="message" placeholder="Message"></textarea>
                                    </div>
                                    <div>
                                        <input type="submit" class="btn-default" value="Submit"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    <?php else : ?>
                        <div class="row espacio">
                            <div class="alert alert-danger" role="alert">
                                <h4 class="titulo-generico danger">You must be logged to make a comment</h4>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- Comments -->
                    <h2>Comments</h2>
                    <div id="comentarios">
                        <?php if (count($comments) !== 0) : ?>
                            <?php foreach ($comments as $comment) : ?>
                                <div class="comment-post">
                                    <div><img src="<?= Comment::getUsuario($comment->getIDUSER())->getUrlAvatar() ?>"
                                              class="comment-img"></div>
                                    <div class="comment-text" id="<?= $comment->getID() ?>">
                                        <h4><?= Comment::getUsuario($comment->getIDUSER())->getName() ?></h4>
                                        <h6><?= Utils::sacarFechaCorta($comment->getFECHA()) ?></h6>
                                        <h5><?= $comment->getTEXT() ?></h5>
                                        <div id="replyComment" class="comment-text comentario">
                                        </div>
                                        <?php if (!is_null($app['user'])) : ?>
                                            <form class="form-inline" action="/delete/comment" method="post"
                                                  enctype="multipart/form-data">
                                                <input type="text" name="idP" class="hidden"
                                                       value="<?= $evento->getId() ?>"/>
                                                <input type="text" name="idC" class="hidden"
                                                       value="<?= $comment->getID() ?>"/>
                                                <input type="text" name="idU" class="hidden"
                                                       value="<?= $app['user']->getId() ?>"/>
                                                <button class="btn btn-secondary" type="button" id="reply">Reply
                                                </button>
                                                <button class="btn btn-danger espacio" type="submit" id="delete">
                                                    Delete
                                                </button>
                                            </form>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php foreach ($replys as $reply) : ?>
                                    <?php if ($reply->getREPLYID() == $comment->getID()) : ?>
                                        <div class="comment-post-right">
                                            <div>
                                                <img src="<?= Comment::getUsuario($reply->getIDUSER())->getUrlAvatar() ?>"
                                                     class="comment-img"></div>
                                            <div class="comment-text">
                                                <h4><?= Comment::getUsuario($reply->getIDUSER())->getName() ?></h4>
                                                <h6><?= Utils::sacarFechaCorta($reply->getFECHA()) ?></h6>
                                                <h5><?= $reply->getTEXT() ?></h5>
                                                <div id="replyComment" class="comment-text comentario">
                                                </div>
                                                <?php if (!is_null($app['user'])) : ?>
                                                <form class="form-inline" action="/delete/comment" method="post"
                                                      enctype="multipart/form-data">
                                                    <input type="text" name="idP" class="hidden"
                                                           value="<?= $evento->getId() ?>"/>
                                                    <input type="text" name="idC" class="hidden"
                                                           value="<?= $comment->getID() ?>"/>
                                                    <input type="text" name="idU" class="hidden"
                                                           value="<?= $app['user']->getId() ?>"/>
                                                    <input type="text" name="idR" class="hidden"
                                                           value="<?= $reply->getID() ?>"/>
                                                    <button class="btn btn-secondary" type="button" id="reply">Reply
                                                    </button>
                                                    <button class="btn btn-danger espacio" type="submit" id="delete">
                                                        Delete
                                                    </button>
                                                    <?php endif; ?>
                                                </form>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="clear"></div>
<script src="/js/reply.js"></script>