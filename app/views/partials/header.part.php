<?php

use \dwes\app\utils\Utils;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Asociation - ACME</title>
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="/css/icons.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/css/easy-responsive-tabs.css " />
    <link rel="stylesheet" type="text/css" href="/css/flexslider.css " />
    <link rel="stylesheet" type="text/css" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/generic.css" />
    <!--[if lt IE 8]><!-->
    <link rel="stylesheet" href="/css/ie7/ie7.css">
    <!--<![endif]-->
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target=".navbar-fixed-top">
<header>
    <nav class="navbar-custom" role="navigation">
        <div class="container">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="index.php">
                    Asociation<span style="width: 60px"> ACME</span>
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-main-collapse ">
                <ul class="nav navbar-nav navbar-right">
                    <div class="posicion-idioma">
                        <div class="form-group">
                            <label for="password"><?= _('Choose your language') ?></label>
                            <div class="row espacio-pequeño">
                                <a href="/idioma/1" name="language"> <img alt="Bandera de España" src="//www.banderas-mundo.es/data/flags/mini/es.png" width="40" height="20" /> ESPAÑOL</a>
                            </div>
                            <div class="row espacio-pequeño">
                                <a href="/idioma/2" name="language"> <img alt="Bandera de Reino Unido" src="//www.banderas-mundo.es/data/flags/mini/gb.png" width="40" height="20" /> INGLES</a>
                            </div>
                        </div>
                    </div>
                    <li>
                        <a href="/" <?php Utils::isActiveMenu('home') ? 'class="active"': '' ?>>Home</a>
                    </li>
                    <?php if (is_null($app['user'])) : ?>
                        <li>
                            <a href="/login" class="btn-default" <?php Utils::isActiveMenu('login') ? 'class="active"': '' ?>>Login</a>
                        </li>
                        <li>
                            <a href="/register" class="btn-default" <?php Utils::isActiveMenu('register') ? 'class="active"': '' ?>>Register</a>
                        </li>
                    <?php else : ?>
                        <?php if ($app['user']->getRole() == 'ROLE_ADMIN') : ?>
                            <li>
                                <a href="/my-elements" <?php Utils::isActiveMenu('list') ? 'class="active"': '' ?>>My Elements</a>
                            </li>
                            <li>
                                <a href="/my-messages" <?php Utils::isActiveMenu('messages') ? 'class="active"': '' ?>>My Messages</a>
                            </li>
                            <li>
                                <a href="/create-element" <?php Utils::isActiveMenu('create') ? 'class="active"': '' ?>>Create Post</a>
                            </li>
                            <li>
                                <a href="/users-list" <?php Utils::isActiveMenu('userList') ? 'class="active"': '' ?>>Users List</a>
                            </li>
                            <li>
                                <a href="/my-profile" class="buttonProfile" <?php Utils::isActiveMenu('myprofile') ? 'class="active"': '' ?>>My Profile</a>
                            </li>
                        <?php else : ?>
                            <li>
                                <a href="/my-elements" <?php Utils::isActiveMenu('postlist') ? 'class="active"': '' ?>>Posts List</a>
                            </li>
                            <li>
                                <a href="/my-messages" <?php Utils::isActiveMenu('messages') ? 'class="active"': '' ?>>My Messages</a>
                            </li>
                            <li>
                                <a href="/create-element" class="button-info" <?php Utils::isActiveMenu('create') ? 'class="active"': '' ?>>Create Post</a>
                            </li>
                            <li>
                                <a href="/my-profile" <?php Utils::isActiveMenu('myprofile') ? 'class="active"': '' ?>>My Profile</a>
                            </li>
                        <?php endif; ?>
                        <li>
                            <a href="/logout" class="btn-default" <?php Utils::isActiveMenu('logout') ? 'class="active"': '' ?>>Logout</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
</header>