<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-5">
                <h2>Asociation<span> ACME</span></h2>
                <div class="row">
                    <div class="col-sm-6">
                        <p>E104 Dharti -2 , Nr Silverstar Mall Chandlodia - Ahmedabad
                            <br/>Zip - 382481</p>
                    </div>
                    <div class="col-sm-6">
                        <ul>
                            <li><a href="#"><i class="fa fa-phone"></i> +91 123 456 7890</a></li>
                            <li><a href="#"><i class="ti-email"></i> info@bootstrapmart.com</a></li>
                        </ul>
                    </div>
                </div>
                <a href="#" class="btn-default">Contact Us</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <ul class="footer-nav">
                    <li><a href="/">Home</a></li>
                    <li><a href="/about">About Us</a></li>
                    <li><a href="/contact">Contact us</a></li>
                </ul>
            </div>
            <div class="col-md-2 col-lg-2 col-md-offset-2 col-lg-offset-2">
                <ul class="footer-social">
                    <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                    <li><a href="#"><i class="fab fa-google-plus-square"></i></a></li>
                    <li><a href="#"><i class="fab fa-twitter-square"></i></a></li>
                    <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fab fa-pinterest-square"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6">
                    Copyright &copy; 2018 distributed by <a href="https://themewagon.com/">ThemeWagon</a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 text-right">
                    <a href="#">Terms & Conditions</a>
                    <a href="#">Policy</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-hover-dropdown/2.2.1/bootstrap-hover-dropdown.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script src="/js/jquery.flexslider-min.js"></script>
<script src="/js/easyResponsiveTabs.js"></script>
<script src="/js/owl.carousel.js"></script>
<script src="/js/custom.js"></script>
<script src="/js/delete.js"></script>
</body>

</html>
