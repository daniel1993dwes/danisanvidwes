<div class="clear"></div>
<div id="page-content">
    <section class="breadcrumb">
        <div class="container">
            <h2>Sent a Message</h2>
        </div>
    </section>
    <section class="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-10">
                    <?php if ($user != null) : ?>
                        <form
                                id="form-register"
                                class="col-mt-6"
                                action="/new/message"
                                method="post"
                                enctype="multipart/form-data">
                            <legend>Send a Message to the autor</legend>
                            <div id="comentarios">
                                <div class="comment-post">
                                    <div><img src="<?= $user->getUrlAvatar() ?>" class="comment-img"></div>
                                    <div class="comment-text">
                                        <h4><?= $user->getName() ?></h4>
                                        <h5 class="titulo-generico"><?= $user->getApellidos() ?></h5>
                                        <h5 class="titulo-generico"><?= $user->getProvincia() ?></h5>
                                        <h5 class="titulo-generico"><?= $user->getEmail() ?></h5>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($error) && !empty($error)) : ?>
                                <div class="row">
                                    <div class="alert alert-danger" role="alert">
                                        <?= $error ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <input type="text" name="idRecive" class="hidden" value="<?= $user->getId() ?>"/>
                            <input type="text" name="idUser" class="hidden" value="<?= $app['user']->getId() ?>"/>
                            <input type="text" name="idComment" class="hidden" value="-1"/>
                            <div class="form-group">
                                <label for="postEvent">Post Text:</label>
                                <textarea class="form-control" id="postText" name="message"
                                          placeholder="Write post text..."></textarea>
                            </div>
                            <a class="btn btn-secondary" href="/" role="button">Go Home</a>
                            <button type="submit" class="btn btn-danger">Send Message</button>
                        </form>
                    <?php else : ?>
                        <img src="/images/gallery/NotFound.jpg">
                        <h1>404 - Not found</h1>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="clear"></div>