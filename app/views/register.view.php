<div class="clear"></div>
<div id="page-content">
    <section class="breadcrumb">
        <div class="container">
            <h2>REGISTER</h2>
            <ul>
                <li><a href="index.php">Home</a> ></li>
                <li><a href="#">Register</a></li>
            </ul>
        </div>
    </section>
    <section class="blog-page">
        <div class="container">
            <?php if (isset($error) && !empty($error)) : ?>
                <div class="row">
                    <div class="alert alert-danger" role="alert">
                        <?= $error ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (isset($nuevoUsuario) && !empty($nuevoUsuario)) : ?>
                <div class="row">
                    <div class="alert alert-success" role="alert">
                        <?= $nuevoUsuario ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-sm-6">
                <form
                        id="form-register"
                        class="mt-4"
                        action="/new-register"
                        method="post"
                        enctype="multipart/form-data">
                    <legend>Create an account</legend>
                    <div class="form-group">
                        <label for="nombre">Name:</label>
                        <input type="text" class="form-control" id="name" name="nameUser" placeholder="Name"
                               value="<?= $nameUser ?>">
                    </div>
                    <div class="form-group">
                        <label for="apellidos">Apellidos:</label>
                        <input type="text" class="form-control" id="apellidos" name="ScNameUser"
                               placeholder="Second and Third Name" value="<?= $ScNameUser ?>">
                    </div>
                    <div class="form-group">
                        <label for="provincia">Provincia:</label>
                        <select name="provincia">
                            <option value="Alicante" <?php if ($provincia == 'Alicante') echo 'selected' ?>>Alicante
                            </option>
                            <option value="Valencia" <?php if ($provincia == 'Valencia') echo 'selected' ?>>Valencia
                            </option>
                            <option value="Castellon" <?php if ($provincia == 'Castellon') echo 'selected' ?>>
                                Castellon
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="correo">Email:</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                               value="<?= $email ?>">
                    </div>
                    <div class="form-group">
                        <label for="correo">Repeat Email:</label>
                        <input type="email" class="form-control" id="email2" name="email2" placeholder="Email"
                               value="<?= $email ?>">
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password"
                               value="<?= $password ?>">
                    </div>
                    <a class="btn btn-secondary" href="/" role="button">Go back</a>
                    <button type="submit" class="btn btn-danger">Create account</button>
                </form>
            </div>
        </div>
    </section>
</div>
<div class="clear"></div>
