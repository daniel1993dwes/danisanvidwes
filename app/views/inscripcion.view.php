<div class="clear"></div>
<div id="page-content">
    <section class="breadcrumb">
        <div class="container">
            <h2>Get Inscription</h2>
        </div>
    </section>
    <section class="blog-page">
        <div class="container">
            <?php if (isset($error) && !empty($error)) : ?>
                <div class="row">
                    <div class="alert alert-danger" role="alert">
                        <?= $error ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (isset($inscription) && !empty($inscription)) : ?>
                <div class="row">
                    <div class="alert alert-success" role="alert">
                        <?= $inscription ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-sm-10">
                    <form
                            id="form-register"
                            class="col-mt-6"
                            action="/new/inscripcion"
                            method="post"
                            enctype="multipart/form-data">
                        <legend>Create a Post</legend>

                        <div class="form-group">
                            <label for="titleEvent">Email:</label>
                            <input type="email" class="form-control" id="title" name="email"
                                   placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="titleEvent">Name:</label>
                            <input type="text" class="form-control" id="title" name="name"
                                   placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="titleEvent">Second Name:</label>
                            <input type="text" class="form-control" id="title" name="apellidos"
                                   placeholder="Second Name">
                            <input type="text" class="hidden" id="title" name="evento"
                                   value="<?= $id_event ?>">
                        </div>
                        <a class="btn btn-secondary" href="/" role="button">Go back</a>
                        <button type="submit" class="btn btn-danger">Get Inscription</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="clear"></div>
<script src="/js/postsButtons.js"></script>
