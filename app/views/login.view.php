<div class="clear"></div>
<div id="page-content">
    <section class="breadcrumb">
        <div class="container">
            <h2>LOGIN</h2>
        </div>
    </section>
    <section class="blog-page">
        <div class="container">
            <?php if (isset($loginError) && !empty($loginError)) : ?>
                <div class="row">
                    <div class="alert alert-danger" role="alert">
                        <?= $loginError ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-sm-4">
                    <form
                            id="form-login"
                            class="mt-4"
                            action="/check-login"
                            method="post">
                        <legend>Welcome to ACME Page</legend>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" name="password" id="password"
                                   placeholder="Password">
                        </div>
                        <div class="active">
                            <a href="#"><p>Have you lose your password?</p></a>
                        </div>
                        <a class="btn btn-secondary" href="/register" role="button">Create account</a>
                        <button type="submit" class="btn btn-danger">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="clear"></div>
