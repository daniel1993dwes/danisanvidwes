<div class="clear"></div>
<div id="page-content">
    <section class="breadcrumb">
        <div class="container">
            <h2>Add Element</h2>
        </div>
    </section>
    <section class="blog-page">
        <div class="container">
            <?php if (isset($error) && !empty($error)) : ?>
                <div class="row">
                    <div class="alert alert-danger" role="alert">
                        <?= $error ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (isset($nuevo_evento) && !empty($nuevo_evento)) : ?>
                <div class="row">
                    <div class="alert alert-success" role="alert">
                        <?= $nuevo_evento ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-sm-10">
                    <form
                            id="form-register"
                            class="col-mt-6"
                            action="/element/new"
                            method="post"
                            enctype="multipart/form-data">
                        <legend>Create a Post</legend>

                        <div class="form-group">
                            <label for="titleEvent">Title:</label>
                            <input type="text" class="form-control" id="title" name="titleEvent"
                                   placeholder="Title" value="<?= $titulo ?>">
                        </div>
                        <div class="form-group">
                            <label for="postEvent">Post Text:</label>
                            <textarea class="form-control" id="postText" name="postEvent"
                                      placeholder="Write post text..."><?= $texto ?></textarea>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="h2">First Title</a>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="h3">Second Title</a>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="parr">Paragraph</a>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="span">Span</a>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="i">Italic</a>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="sub">Underlined</a>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="neg">Bold</a>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="listDes">List</a>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="quote">Quote</a>
                            <a class="btn btn-primary botonAdd" href="#" role="button" id="spc">Special Chars</a>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-3">
                                <label for="fecha">Date of the Event:</label>
                                <input type="date" class="form-control" id="date" name="dateEvent"
                                       placeholder="Date" value="<?= $fecha ?>">
                            </div>
                            <div class="form-group col-sm-2">
                                <label for="fecha">Hour of the Event:</label>
                                <input type="time" class="form-control" id="time" name="timeEvent"
                                       placeholder="Date" value="<?= $hora ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label for="tipo">Type of the de Event:</label>
                                <select name="tipo">
                                    <option value="EVENTO GENERICO" <?php if ($tipo == 'EVENTO GENERICO') echo 'selected' ?>>
                                        GENERIC EVENT
                                    </option>
                                    <option value="EVENTO COSPLAY" <?php if ($tipo == 'EVENTO COSPLAY') echo 'selected' ?>>
                                        COSPLAY EVENT
                                    </option>
                                    <option value="EVENTO VIDEOJUEGOS" <?php if ($tipo == 'EVENTO VIDEOJUEGOS') echo 'selected' ?>>
                                        VIDEOGAMES EVENT
                                    </option>
                                    <option value="EVENTO DE ROL EN VIVO" <?php if ($tipo == 'EVENTO DE ROL EN VIVO') echo 'selected' ?>>
                                        EVENT OF ROLE PLAY
                                    </option>
                                    <option value="EVENTO PROYECCIÓN DE PELÍCULA" <?php if ($tipo == 'EVENTO PROYECCIÓN DE PELÍCULA') echo 'selected' ?>>
                                        EVENT OF SHOW FILM
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="alert alert-success" role="alert">
                                Please set the Gauging of the event
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-2">
                                <label for="imageEvent">Gauging: </label>
                                <input type="number" class="form-control" name="aforo" min="1" max="9999" value="<?= $aforo ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="alert alert-success" role="alert">
                                Please set the image of the event
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imageEvent">Post Event Image</label>
                            <input type="file" class="form-control" id="image" name="imageEvent">
                        </div>
                        <a class="btn btn-secondary" href="/" role="button">Go back</a>
                        <button type="submit" class="btn btn-danger">Create Post</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="clear"></div>
<script src="/js/postsButtons.js"></script>
