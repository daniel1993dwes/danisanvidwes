<?php

use dwes\app\entity\Message;
use dwes\app\utils\Utils;

?>

<div class="clear"></div>
<div id="page-content">
    <section class="breadcrumb">
        <div class="container">
            <h2>My Messages</h2>
        </div>
    </section>
    <section class="blog-page">
        <div class="container">
            <nav class="navbar navbar-light bg-light">
                <div class="col-sm-4">
                    <div class="row">
                        <form class="form-group"
                              action="/find-user"
                              method="post"
                              enctype="multipart/form-data">
                            <label for="find">Find User: </label>
                            <input type="text" name="nameUser" placeholder="Name user"/>
                            <div class="form-group">
                                <label for="provincia">Provincia:</label>
                                <select name="provincia">
                                    <option value="" selected>Select Region
                                    </option>
                                    <option value="Alicante">Alicante
                                    </option>
                                    <option value="Valencia">Valencia
                                    </option>
                                    <option value="Castellon">Castellon
                                    </option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-danger espacio-pequeño">Find User</button>
                        </form>
                    </div>
                </div>
            </nav>
            <?php if (isset($error) && !empty($error)) : ?>
                <div class="row">
                    <div class="alert alert-danger" role="alert">
                        <?= $error ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (isset($filter_user) && !empty($filter_user)) : ?>
                <div class="row">
                    <div class="alert alert-success" role="alert">
                        <?= $filter_user ?>
                    </div>
                </div>
                <?php if (count($users) > 0) : ?>
                    <table class="table">
                        <thead></thead>
                        <tr>
                            <th scope="col">
                                Nombre
                            </th>
                            <th scope="col">
                                Provincia
                            </th>
                            <th scope="col">
                                Enviar Mensaje
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $user) : ?>
                            <tr scope="row">
                                <td class="color-texto">
                                    <?= $user->getName() ?>
                                </td>
                                <td class="color-texto">
                                    <?= $user->getProvincia() ?>
                                </td>
                                <td>
                                    <a href="/sent/<?= $user->getId() ?>" class="btn btn-primary">Sent a Message</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </section>
</div>

<section class="blog-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h2>Messages</h2>
                <h3>Received</h3>
                <div id="comentarios">
                    <?php if (count($recividos) !== 0) : ?>
                        <?php foreach ($recividos as $recivido) : ?>
                            <div class="comment-post">
                                <div><img src="<?= Message::getUsuario($recivido->getREMITENTE())->getUrlAvatar() ?>"
                                          class="comment-img"></div>
                                <div class="comment-text" id="<?= $recivido->getID() ?>">
                                    <h4>DE: <?= Message::getUsuario($recivido->getREMITENTE())->getName() ?></h4>
                                    <h6><?= Utils::sacarFechaCorta($recivido->getFECHA()) ?></h6>
                                    <h5><?= $recivido->getMENSAJE() ?></h5>
                                    <div id="replyComment" class="comment-text comentario">
                                    </div>
                                    <form class="form-inline" action="/delete/message" method="post"
                                          enctype="multipart/form-data">
                                        <input type="text" name="idU" class="hidden"
                                               value="<?= $app['user']->getId() ?>"/>
                                        <input type="text" name="idR" class="hidden"
                                               value="<?= $recivido->getREMITENTE() ?>"/>
                                        <input type="text" name="idC" class="hidden"
                                               value="<?= $recivido->getID() ?>"/>
                                        <button class="btn btn-secondary" type="button" id="reply">Reply
                                        </button>
                                    </form>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <h3>Delivered</h3>
                    <?php foreach ($enviados as $enviado) : ?>
                            <div class="comment-post-right">
                                <div>
                                    <img src="<?= Message::getUsuario($enviado->getDESTINATARIO())->getUrlAvatar() ?>"
                                         class="comment-img"></div>
                                <div class="comment-text">
                                    <h4>PARA: <?= Message::getUsuario($enviado->getDESTINATARIO())->getName() ?></h4>
                                    <h6><?= Utils::sacarFechaCorta($enviado->getFECHA()) ?></h6>
                                    <h5><?= $enviado->getMENSAJE() ?></h5>
                                    <div id="replyComment" class="comment-text comentario">
                                    </div>
                                    <?php if (!is_null($app['user'])) : ?>
                                    <form class="form-inline" action="/delete/message" method="post"
                                          enctype="multipart/form-data">
                                        <input type="text" name="idU" class="hidden"
                                               value="<?= $app['user']->getId() ?>"/>
                                        <input type="text" name="idR" class="hidden"
                                               value="<?= $enviado->getREMITENTE() ?>"/>
                                        <button class="btn btn-danger espacio" type="submit" id="delete">
                                            Delete
                                        </button>
                                        <?php endif; ?>
                                    </form>
                                </div>
                            </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clear"></div>
<script src="/js/replyMessage.js"></script>
