<?php

namespace dwes\app\utils;

class Utils
{
    public static function isActiveMenu(string $menuOption) : bool
    {
        return (strpos($_SERVER['REQUEST_URI'], $menuOption) !== false);
    }

    public static function convertirFechaES(string $fecha) : string
    {
        list($ano, $mes, $tiempo) = explode('-', $fecha);
        list($dia, $reloj) = explode(' ', $tiempo);
        list($hora, $minuto, $segundo) = explode(':', $reloj);
        $Nfecha = "$dia-$mes-$ano $hora:$minuto:$segundo";
        return $Nfecha;
    }
    
    public static function sacarResumen(string $texto) : string
    {
        $nuevo_texto = substr($texto, 0, 200);
        return $nuevo_texto . '...';
    }

    public static function sacarResumenPequeño(string $texto) : string
    {
        $nuevo_texto = substr($texto, 0, 10);
        return $nuevo_texto . '...';
    }

    public static function sacarResumenLogger(string $texto) : string
    {
        $nuevo_texto = substr($texto, 0, 50);
        return $nuevo_texto . '...';
    }

    public static function sacarFechaCorta(string $fecha) : string
    {
        list($ano, $mes, $tiempo) = explode('-', $fecha);
        list($dia, $reloj) = explode(' ', $tiempo);
        list($hora, $minuto, $segundo) = explode(':', $reloj);
        $Nfecha = "$dia-$mes-$ano";
        return $Nfecha;
    }
    public static function sacarFechaSql(string $fecha) : string
    {
        list($ano, $mes, $tiempo) = explode('-', $fecha);
        list($dia, $reloj) = explode(' ', $tiempo);
        list($hora, $minuto, $segundo) = explode(':', $reloj);
        $Nfecha = "$ano-$mes-$dia";
        return $Nfecha;
    }
    public static function sacarHora(string $fecha) : string
    {
        list($ano, $mes, $tiempo) = explode('-', $fecha);
        list($dia, $reloj) = explode(' ', $tiempo);
        list($hora, $minuto, $segundo) = explode(':', $reloj);
        $Nfecha = "$hora:$minuto:$segundo";
        return $Nfecha;
    }
}