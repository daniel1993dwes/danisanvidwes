<?php

namespace dwes\app\controllers;

use dwes\app\repository\InscripcionesDB;
use dwes\app\repository\MessageDB;
use dwes\app\repository\PostDB;
use dwes\app\repository\UsuarioDB;
use dwes\core\App;
use dwes\core\helpers\FlashMessage;
use dwes\core\helpers\VariablePersistent;
use dwes\core\Response;
use dwes\core\Security;

class UsuarioController
{
    public function GetUsuarios()
    {
        $sql = VariablePersistent::get('sql');
        $parametros = VariablePersistent::get('parametros');
        $filter_user = FlashMessage::get('filter-apply');
        $error = FlashMessage::get('error');
        if ($parametros)
        {
            $users = App::getRepository(UsuarioDB::class)->executeSql($sql, $parametros);
        }
        else
        {
            $users = App::getRepository(UsuarioDB::class)->findAll();
        }
        $users ?? [];

        $data = compact('users', 'filter_user', 'error');
        Response::renderView('users-list',
            $data);
    }

    public function FindUser()
    {
        $parametros = [];
        $sql = "SELECT * FROM USUARIO WHERE 1 = 1";
        if (isset($_POST['tipo']) && !empty($_POST['tipo']) && empty($_POST['nameUser']))
        {
            $sql .= ' AND UPPER(ROLE) LIKE :ROLE';
            $parametros[':ROLE'] = '%'.trim(htmlspecialchars(strtoupper($_POST['tipo']))).'%';
            FlashMessage::set('filter-apply', 'Filter was apply');
        }
        if (!empty($_POST['nameUser']) && isset($_POST['nameUser']) && empty($_POST['tipo']))
        {
            $sql .= ' AND UPPER(NAME) LIKE :NAME';
            $parametros[':NAME'] = '%'.trim(htmlspecialchars(strtoupper($_POST['nameUser']))).'%';
            FlashMessage::set('filter-apply', 'Filter was apply');
        }
        if (isset($_POST['tipo']) && !empty($_POST['tipo']) && !empty($_POST['nameUser']) && isset($_POST['nameUser']))
        {
            $sql .= ' AND UPPER(ROLE) LIKE :ROLE AND UPPER(NAME) LIKE :NAME';
            $parametros[':ROLE'] = '%'.trim(htmlspecialchars(strtoupper($_POST['tipo']))).'%';
            $parametros[':NAME'] = '%'.trim(htmlspecialchars(strtoupper($_POST['nameUser']))).'%';
            FlashMessage::set('filter-apply', 'Filter was apply');
        }
        if (count($parametros) == 0)
        {
            FlashMessage::set('error', 'Fields was empty');
        }
        VariablePersistent::set('sql', $sql);
        VariablePersistent::set('parametros', $parametros);
        App::get('router')->redirect('users-list');
    }

    public function ChangeRole()
    {
        if (isset($_POST['tipo']) && !empty($_POST['tipo']))
        {
            $user = App::getRepository(UsuarioDB::class)->find($_POST['idUser']);
            $user->setRole($_POST['tipo']);
            App::getRepository(UsuarioDB::class)->update($user);
            FlashMessage::set('filter-apply', 'The update was successful');
        }
        else
        {
            FlashMessage::set('error', 'An error has ocurred when update the user');
        }
        App::get('router')->redirect('users-list');
    }

    public function DeleteUser(int $id_user)
    {
        $sql_eventos = "SELECT COUNT(*) AS 'LISTA' FROM POST WHERE ID_USUARIO = " . $id_user;
        $sql_mensajes = "SELECT COUNT(*) AS 'LISTA' FROM MESSAGE WHERE REMITENTE = " . $id_user . " AND DESTINATARIO = " . $id_user;
        $sql_inscripciones = "SELECT COUNT(*) AS 'LISTA' FROM INSCRIPCIONES WHERE ID_USUARIO = " . $id_user;

        $count_eventos = App::getRepository(PostDB::class)->executeSqlGeneric($sql_eventos);
        $count_mensajes = App::getRepository(MessageDB::class)->executeSqlGeneric($sql_mensajes);
        $count_inscripciones = App::getRepository(InscripcionesDB::class)->executeSqlGeneric($sql_inscripciones);

        if ($count_eventos[0]['LISTA'] == 0 && $count_mensajes[0]['LISTA'] == 0 && $count_inscripciones[0]['LISTA'] == 0)
        {
            App::getRepository(UsuarioDB::class)->delete($id_user);
            FlashMessage::set('filter-apply', 'The user has been deleted');
        }
        else
        {
            FlashMessage::set('error', 'The user have events, messages or has inscriptions');
        }
        App::get('router')->redirect('users-list');
    }

    public function ChangePassword()
    {
        if (isset($_POST['pas1'])
        && !empty($_POST['pas1'])
        && isset($_POST['pas2'])
        && !empty($_POST['pas2']))
        {
            $user = App::getRepository(UsuarioDB::class)->find($_POST['id']);
            if ($_POST['pas1'] == $_POST['pas2'])
            {
                $user->setPassword(trim(htmlspecialchars(Security::encrypt($_POST['pas1']))));
                App::getRepository(UsuarioDB::class)->update($user);
                FlashMessage::set('nuevo', 'Password has been changed successful');
            }
            else
            {
                FlashMessage::set('error', 'The both password must be equal');
            }
        }
        else
        {
            FlashMessage::set('error', 'The fields are empty');
        }
        App::get('router')->redirect('my-profile');
    }
    public function ChangeAvatar()
    {
        if ($_FILES['image']['size'] > 0 && $_FILES['image']['size'] < 10000)
        {
            $user = App::getRepository(UsuarioDB::class)->find($_POST['id']);
            try
            {
                if ($user->getAvatar() != 'userDefault.jpg')
                {
                    unlink('./images/users/' . $user->getAvatar());
                }
                $user->setField($_FILES['image'])
                    ->setAcceptedTypes(
                        [
                            'image/jpg',
                            'image/png',
                            'image/jpeg'
                        ]
                    )
                    ->setTargetDirectory('./images/users/')
                    ->setError();
                $nombre_imagen = $user->getFileName();
                if ($user->executeUpload() === true)
                {
                    $user->setAvatar($nombre_imagen);
                    App::getRepository(UsuarioDB::class)->update($user);
                    FlashMessage::set('nuevo', 'Avatar is changed successful');
                }
                else
                {
                    FlashMessage::set('error', $user->getLastError());
                }
            }
            catch (\Exception $exception)
            {
                FlashMessage::set('error', $exception->getMessage());
            }
        }
        else
        {
            FlashMessage::set('error', 'Set a image to upload Max 10Kb');
        }

        App::get('router')->redirect('my-profile');
    }

    public function ChargeUserProfile()
    {
        $error = FlashMessage::get('error');
        $nuevo = FlashMessage::get('nuevo');
        $id_user = App::get('user')->getId();
        $user = App::getRepository(UsuarioDB::class)->find($id_user);

        $sql = "SELECT * FROM INSCRIPCIONES WHERE ID_USUARIO = $id_user";
        $events = App::getRepository(InscripcionesDB::class)->executeSql($sql);
        $events ?? [];

        Response::renderView('my-profile',
            compact('user', 'events', 'error', 'nuevo'));
    }
}