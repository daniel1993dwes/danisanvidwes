<?php

namespace dwes\app\controllers;

use dwes\app\repository\MessageDB;
use dwes\core\helpers\FlashMessage;
use dwes\core\helpers\FormPersistent;
use dwes\core\helpers\VariablePersistent;
use dwes\core\Response;
use dwes\core\App;
use dwes\app\repository\UsuarioDB;
use dwes\app\entity\Message;

class MessageController
{
    public function Sent(int $id_user)
    {
        $user = App::getRepository(UsuarioDB::class)->find($id_user);
        $user ?? null;
        $error = FlashMessage::get('error');

        Response::renderView('sent',
            compact('user', 'error', 'id_user'));
    }

    public function NewMessage()
    {
        $message = new Message();

        if (isset($_POST['message']) && !empty($_POST['message'])) {
            $message->setDESTINATARIO(trim(htmlspecialchars($_POST['idRecive'])));
            $message->setREMITENTE(trim(htmlspecialchars($_POST['idUser'])));
            $message->setMENSAJE(trim(htmlspecialchars($_POST['message'])));

            App::getRepository(MessageDB::class)->insert($message);
            FlashMessage::set('nuevo-mensaje', 'The message was sent');
        } else {
            FlashMessage::set('error', 'The message is empty');
        }
        App::get('router')->redirect('my-messages');
    }

    public function Messages()
    {
        $users = [];
        $id_user = App::get('user')->getId();
        $sql = VariablePersistent::get('sql');
        $parameters = VariablePersistent::get('parameters');
        $filtro = VariablePersistent::get('filtro');
        $filter_user = FlashMessage::get('filter-apply');
        $error = FlashMessage::get('error');
        $nuevo_mensaje = FlashMessage::get('nuevo-mensaje');

        if ($filtro)
        {
            $users = App::getRepository(UsuarioDB::class)->executeSql($sql, $parameters);
        }
        $sql_recividos = "SELECT * FROM MESSAGE WHERE DESTINATARIO = :DESTINATARIO ORDER BY FECHA DESC";
        $sql_enviados = "SELECT * FROM MESSAGE WHERE REMITENTE = :REMITENTE ORDER BY FECHA DESC";
        $recividos = App::getRepository(MessageDB::class)->executeSql($sql_recividos, [':DESTINATARIO' => $id_user]);
        $recividos ?? [];
        $enviados = App::getRepository(MessageDB::class)->executeSql($sql_enviados, [':REMITENTE' => $id_user]);
        $enviados ?? [];
        $users ?? [];

        $data = compact('error', 'filter_user', 'users', 'recividos', 'enviados', 'nuevo_mensaje');
        Response::renderView(
            'my-messages',
            $data);
    }

    public function DeleteMessage()
    {
        if (isset($_POST['idC']) && !empty($_POST['idC']))
        {
            App::getRepository(MessageDB::class)->delete($_POST['idC']);
        }
        else
        {
            FlashMessage::set(
                'error-comentario', 'You can not delete comments that have been replicated');
        }
        App::get('router')->redirect('my-messages');
    }

    public function FindUser()
    {
        $sql = 'SELECT * FROM USUARIO WHERE 1 = 1';
        $parametros = [];
        if (isset($_POST['nameUser']) && !empty($_POST['nameUser']) && empty($_POST['provincia']))
        {
            $sql .= ' AND UPPER(NAME) LIKE :NAME';
            $parametros[':NAME'] = "%".trim(htmlspecialchars(strtoupper($_POST['nameUser'])))."%";
            FlashMessage::set('filter-apply', 'Filter users');
        }
        if (isset($_POST['provincia']) && !empty($_POST['provincia']) && empty($_POST['nameUser']))
        {
            $sql .= ' AND UPPER(PROVINCIA) = :PROVINCIA';
            $parametros[':PROVINCIA'] = trim(htmlspecialchars(strtoupper($_POST['provincia'])));
            FlashMessage::set('filter-apply', 'Filter users');
        }
        if (isset($_POST['nameUser']) && !empty($_POST['nameUser']) && isset($_POST['provincia']) && !empty($_POST['provincia']))
        {
            $sql .= ' AND UPPER(NAME) LIKE :NAME AND UPPER(PROVINCIA) = :PROVINCIA';
            $parametros[':NAME'] = "%".trim(htmlspecialchars(strtoupper($_POST['nameUser'])))."%";
            $parametros[':PROVINCIA'] = trim(htmlspecialchars(strtoupper($_POST['provincia'])));
            FlashMessage::set('filter-apply', 'Filter users');
        }
        if ($_POST['nameUser'] == '' && $_POST['provincia'] == '')
        {
            FlashMessage::set('error', 'Form is empty');
        }

        VariablePersistent::set('sql', $sql);
        VariablePersistent::set('parameters', $parametros);
        VariablePersistent::set('filtro', 'filtramos');

        App::get('router')->redirect('my-messages');
    }
}