<?php

namespace dwes\app\controllers;

use dwes\app\entity\Inscripciones;
use dwes\app\repository\InscripcionesDB;
use dwes\core\App;
use dwes\app\repository\PostDB;
use dwes\app\repository\CommentDB;
use dwes\core\helpers\VariablePersistent;
use dwes\core\Response;
use dwes\core\helpers\FormPersistent;
use dwes\core\helpers\FlashMessage;
use dwes\app\entity\Post;
use dwes\app\entity\Comment;
use dwes\app\utils\Utils;

class PostController
{
    public function DeleteEvent(int $id_event)
    {
        $pdo = App::getConnection();

        try
        {
            $pdo->beginTransaction();

            $sql = "SELECT COUNT(*) AS 'LISTA' FROM INSCRIPCIONES WHERE ID_EVENTO = $id_event";
            $count_events = App::getRepository(InscripcionesDB::class)->executeSqlGeneric($sql);
            $event = App::getRepository(PostDB::class)->find($id_event);
            if ($count_events[0]['LISTA'] > 0)
            {
                $event->setVisible(false);
                App::getRepository(PostDB::class)->update($event);
                FlashMessage::set('filtros', 'The event now is invisible');
            }
            else
            {
                App::getRepository(PostDB::class)->delete($id_event);
                FlashMessage::set('filtros', 'The event has been deleted');
            }

            $pdo->commit();
        }
        catch(\PDOException $exception)
        {
            $pdo->rollBack();

            FlashMessage::set(
                'error', $exception->getMessage());
        }
        App::get('router')->redirect('my-elements');
    }

    public function NewInscripcion()
    {
        if (isset($_POST['email'])
            && isset($_POST['name'])
            && isset($_POST['apellidos'])
            && !empty($_POST['email'])
            && !empty($_POST['name'])
            && !empty($_POST['apellidos']))
        {
            $insc = new Inscripciones();
            $pdo = App::getConnection();
            try
            {
                $pdo->beginTransaction();
                $id_user = App::get('user')->getId();

                $insc->setEMAIL(trim(htmlspecialchars($_POST['email'])));
                $insc->setNOMBRE(trim(htmlspecialchars($_POST['name'])));
                $insc->setAPELLIDOS(trim(htmlspecialchars($_POST['apellidos'])));
                $insc->setIDUSUARIO($id_user);
                $insc->setIDEVENTO($_POST['evento']);

                $sql = "SELECT COUNT(*) AS 'LISTA' FROM INSCRIPCIONES WHERE ID_USUARIO = $id_user AND ID_EVENTO = " . $_POST['evento'];
                $count_sql = App::getRepository(InscripcionesDB::class)->executeSqlGeneric($sql);
                if ($count_sql[0]['LISTA'] > 0)
                {
                    $pdo->rollBack();
                    FlashMessage::set('error', 'You are already enrolled in that event');
                }
                else
                {
                    App::getRepository(InscripcionesDB::class)->insert($insc);

                    $event = App::getRepository(PostDB::class)->find($_POST['evento']);
                    $number = $event->getAFORO();

                    $event->setAFORO($number - 1);
                    App::getRepository(PostDB::class)->update($event);

                    App::getLogger()->addWarning(
                        'Nueva inscripcion de usuario con nombre:' . $_POST['name'] . ' al evento con id:' . $_POST['evento']);

                    FlashMessage::set(
                        'nuevo', 'The enrolled was successful');
                    if ($event->getAFORO() >= 0)
                    {
                        $pdo->commit();
                    }
                    else
                    {
                        $pdo->rollBack();
                        FlashMessage::set('error', 'Gauging is full contact with the Administrator');
                    }
                }
            }
            catch(\PDOException $exception)
            {
                $pdo->rollBack();

                FlashMessage::set(
                    'error', $exception->getMessage());
            }
        }
        else
        {
            FlashMessage::set('error', 'Fields are empty');
        }
        App::get('router')->redirect('get-inscripcion/' . $_POST['evento']);
    }

    public function filtrar(int $page = null)
    {
        $user = App::get('user');
        $parametros = [];
        if ($user->getRole() == 'ROLE_ADMIN')
        {
            $sql = 'SELECT * FROM POST WHERE 1 = 1 ';
        }
        else
        {
            $sql = 'SELECT * FROM POST WHERE ID_USUARIO = ' . $user->getId();
        }
        if (isset($_POST['fecha1']) && isset($_POST['fecha2']) && !empty($_POST['fecha1']) && !empty($_POST['fecha2']))
        {
            if ($_POST['fecha1'] > $_POST['fecha2'])
            {
                $fecha1 = $_POST['fecha2'];
                $fecha2 = $_POST['fecha1'];
            }
            else
            {
                $fecha1 = $_POST['fecha1'];
                $fecha2 = $_POST['fecha2'];
            }
            $parte_sql = ' AND FECHA BETWEEN :FECHA1 AND :FECHA2';
            $sql .= $parte_sql;
            $parametros[':FECHA1'] = trim(htmlspecialchars($fecha1 . ' 00:00:00'));
            $parametros[':FECHA2'] = trim(htmlspecialchars($fecha2 . ' 23:59:59'));
            FlashMessage::set('filtros', 'Filters was apply');
        }
        if (!empty($_POST['fecha1']) && isset($_POST['fecha1']) && empty($_POST['fecha2']))
        {
            $parte_sql = ' AND FECHA >= :FECHA1';
            $sql .= $parte_sql;
            $parametros[':FECHA1'] = trim(htmlspecialchars($_POST['fecha1']));
            FlashMessage::set('filtros', 'Filters was apply');
        }
        if (!empty($_POST['fecha2']) && isset($_POST['fecha2']) && empty($_POST['fecha1']))
        {
            $parte_sql = ' AND FECHA <= :FECHA2';
            $sql .= $parte_sql;
            $parametros[':FECHA2'] = trim(htmlspecialchars($_POST['fecha2']));
            FlashMessage::set('filtros', 'Filters was apply');
        }
        if (isset($_POST['tipo']) && !empty($_POST['tipo']))
        {
            $parte_sql = ' AND UPPER(TIPO) LIKE :TIPO';
            $sql .= $parte_sql;
            $parametros[':TIPO'] = trim(htmlspecialchars(strtoupper($_POST['tipo'])));
            FlashMessage::set('filtros', 'Filters was apply');
        }
        if (isset($_POST['texto']) && !empty($_POST['texto']))
        {
            $parte_sql = ' AND UPPER(TITULO) LIKE :TEXTO';
            $sql .= $parte_sql;
            $parametros[':TEXTO'] = "%".trim(htmlspecialchars(strtoupper($_POST['texto'])))."%";
            FlashMessage::set('filtros', 'Filters was apply');
        }

        VariablePersistent::set('sql_filtros', $sql . ' ORDER BY FECHA DESC');
        VariablePersistent::set('parametros', $parametros);

        App::get('router')->redirect('my-elements');
    }
    
    public function MyElements()
    {
        $user = App::get('user');
        $parametros = VariablePersistent::get('parametros');
        if ($parametros)
        {
            $sql = VariablePersistent::get('sql_filtros');
            
            $elements = App::getRepository(PostDB::class)->executeSql($sql, $parametros);
        }
        else
        {
            if ($user->getRole() == 'ROLE_ADMIN')
            {
                $sql = "SELECT * FROM POST";
            }
            else
            {
                $sql = "SELECT * FROM POST WHERE ID_USUARIO = " . $user->getId();
            }
            $elements = App::getRepository(PostDB::class)->executeSql($sql);
        }
        $filter = FlashMessage::get('filtros');
        $error = FlashMessage::get('error');

        $elements ?? [];
        Response::renderView(
            'my-elements',
            compact('elements', 'filter', 'error')
        );
    }

    public function borrar()
    {
        $idEvento = $_GET['id'];

        if (isset($idEvento))
        {
            App::getRepository(PostDB::class)->delete($idEvento);

            $resultado = true;
        }
        else
            $resultado = false;

        echo json_encode($resultado);
    }

    public function ChargeModifyForm(int $id_event)
    {
        $error = FlashMessage::get('error');

        $event = App::getRepository(PostDB::class)->find($id_event);
        Response::renderView('modify-element', compact('event', 'error'));
    }

    public function ModifyEvent()
    {
        if (isset($_POST['titleEvent'])
            && isset($_POST['postEvent'])
            && isset($_POST['dateEvent'])
            && isset($_POST['timeEvent'])
            && isset($_POST['tipo'])
            && isset($_POST['aforo'])
            && !empty($_POST['titleEvent'])
            && !empty($_POST['postEvent'])
            && !empty($_POST['tipo'])
            && !empty($_POST['dateEvent'])
            && !empty($_POST['aforo'])
            && !empty($_POST['timeEvent'])
            && $_FILES['imageEvent']['size'] > 0)
        {
            $evento = App::getRepository(PostDB::class)->find($_POST['idEvent']);
            if ($evento->getImagen() != 'defaultEvent.png')
            {
                unlink('./images/gallery/' . $evento->getImagen());
            }

            $evento->setField($_FILES['imageEvent'])
                ->setAcceptedTypes(
                    [
                        'image/png',
                        'image/jpg',
                        'image/jpeg'
                    ]
                )
                ->setTargetDirectory('./images/gallery/')
                ->setError();
            $nombre_imagen = $evento->getFileName() ?? '';
            if ($evento->executeUpload() === true)
            {
                $evento->setTitulo(trim(htmlspecialchars($_POST['titleEvent'])));
                $evento->setImagen($nombre_imagen);
                $evento->setTexto(trim(htmlspecialchars($_POST['postEvent'])));
                $evento->setTipo(trim(htmlspecialchars($_POST['tipo'])));
                $evento->setIdUsuario(trim(htmlspecialchars(App::get('user')->getId())));
                $evento->setAFORO(trim(htmlspecialchars($_POST['aforo'])));

                $nueva_fecha = trim(htmlspecialchars($_POST['dateEvent'])) . ' ' . trim(htmlspecialchars($_POST['timeEvent'])) . ':00';
                $evento->setFecha($nueva_fecha);

                try
                {
                    App::getRepository(PostDB::class)->update($evento);
                    FlashMessage::set('filtros', "The event has been modified with name: " . $evento->getTitulo());
                    App::get('router')->redirect('my-elements');
                }
                catch(PDOException $exception)
                {
                    FlashMessage::set('error', $exception->getMessage());
                    App::get('router')->redirect('modify-element/' . $_POST['idEvent']);
                }
            }
            else
            {
                FlashMessage::set('error', $evento->getLastError());
                App::get('router')->redirect('modify-element/' . $_POST['idEvent']);
            }
        }
        else
        {
            FlashMessage::set('error', 'The fields are empty or not stablished');
            App::get('router')->redirect('modify-element/' . $_POST['idEvent']);
        }
    }

    public function crearElemento()
    {
        $titulo = FormPersistent::get('titulo');
        $texto = FormPersistent::get('texto');
        $fecha = FormPersistent::get('fecha');
        $hora = FormPersistent::get('hora');
        $tipo = FormPersistent::get('tipo');
        $aforo = FormPersistent::get('aforo');

        $error = FlashMessage::get('error');
        $nuevo_evento = FlashMessage::get('nuevo-evento');

        $data = compact('titulo', 'texto', 'fecha', 'hora', 'tipo', 'error', 'nuevo_evento', 'aforo');

        Response::renderView(
            'add-element',
            $data
        );

    }

    public function GetInscripcion(int $id_event)
    {
        $error = FlashMessage::get('error');
        $inscription = FlashMessage::get('nuevo');
        Response::renderView(
            'inscripcion',
            compact('error', 'inscription', 'id_event'));
    }

    public function AddComment()
    {
        $comment = new Comment();
        $pdo = App::getConnection();

        if (intval($_POST['idComment']) === -1)
        {
            $comment->setREPLY(false);
            $comment->setREPLYID(-1);
        }
        else
        {
            $comment->setREPLY(true);
            $comment->setREPLYID(trim(htmlspecialchars($_POST['idComment'])));
        }

        if (isset($_POST['message']) && !empty($_POST['message']))
        {
            try
            {
                $pdo->beginTransaction();

                $comment->setIDPOST(trim(htmlspecialchars(intval($_POST['idPost']))));
                $comment->setIDUSER(trim(htmlspecialchars(intval($_POST['idUser']))));
                $comment->setTEXT(trim(htmlspecialchars($_POST['message'])));

                App::getRepository(CommentDB::class)->insert($comment);

                $sql = 'UPDATE POST SET COMMENT_CANT = COMMENT_CANT + 1 WHERE ID = ' . $_POST['idPost'];
                App::getRepository(PostDB::class)->executeSqlNotReturn($sql);

                $pdo->commit();

                App::getLogger()->addWarning(
                    'Nuevo comentario creado del usuario:' . $_POST['idUser'] . ' :' . Utils::sacarResumenLogger($_POST['message']));

                FlashMessage::set(
                    'nuevo-comentario', 'Se ha insertado el comentario');
            }
            catch(PDOException $exception)
            {
                $pdo->rollBack();

                FlashMessage::set(
                    'error-comentario', $exception->getMessage());
            }
        }
        else
        {
            FlashMessage::set(
                'error-comentario', 'No hay comentario a insertar');
        }

        App::get('router')->redirect('details/'. $_POST['idPost']);
    }

    public function nuevoElemento()
    {
        if (isset($_POST['titleEvent'])
        && isset($_POST['postEvent'])
        && isset($_POST['dateEvent'])
        && isset($_POST['timeEvent'])
        && isset($_POST['tipo'])
        && isset($_POST['aforo'])
        && !empty($_POST['titleEvent'])
        && !empty($_POST['postEvent'])
        && !empty($_POST['tipo'])
        && !empty($_POST['dateEvent'])
        && !empty($_POST['aforo'])
        && !empty($_POST['timeEvent']))
        {
            $evento = new Post();
            
            $evento->setField($_FILES['imageEvent'])
                ->setAcceptedTypes(
                    [
                    'image/png',
                    'image/jpg',
                    'image/jpeg'
                    ]
                )
                ->setTargetDirectory('./images/gallery/')
                ->setError();
                $nombre_imagen = $evento->getFileName() ?? '';
            if ($evento->executeUpload() === true)
            {
                $evento->setTitulo(trim(htmlspecialchars($_POST['titleEvent'])));
                $evento->setImagen($nombre_imagen);
                $evento->setTexto(trim(htmlspecialchars($_POST['postEvent'])));
                $evento->setTipo(trim(htmlspecialchars($_POST['tipo'])));
                $evento->setIdUsuario(trim(htmlspecialchars(App::get('user')->getId())));
                $evento->setAFORO(trim(htmlspecialchars($_POST['aforo'])));

                $nueva_fecha = trim(htmlspecialchars($_POST['dateEvent'])) . ' ' . trim(htmlspecialchars($_POST['timeEvent'])) . ':00';
                $evento->setFecha($nueva_fecha);
                
                try
                {
                    App::getRepository(PostDB::class)->insert($evento);
                    FlashMessage::set('nuevo-evento', "Se ha insertado un nuevo evento con título: " . $evento->getTitulo());
                }
                catch(PDOException $exception)
                {
                    FlashMessage::set('error', $exception->getMessage());
                }
            }
            else
            {
                FlashMessage::set('error', $evento->getLastError());

                FormPersistent::set('titulo', trim(htmlspecialchars($_POST['titleEvent'])));
                FormPersistent::set('texto', trim(htmlspecialchars($_POST['postEvent'])));
                FormPersistent::set('fecha', trim(htmlspecialchars($_POST['dateEvent'])));
                FormPersistent::set('hora', trim(htmlspecialchars($_POST['timeEvent'])));
                FormPersistent::set('tipo', trim(htmlspecialchars($_POST['tipo'])));
                FormPersistent::set('aforo', trim(htmlspecialchars($_POST['aforo'])));

            }
        }
        else
        {
            FlashMessage::set('error', 'The fields are empty or not stablished');

            FormPersistent::set('titulo', trim(htmlspecialchars($_POST['titleEvent'])));
            FormPersistent::set('texto', trim(htmlspecialchars($_POST['postEvent'])));
            FormPersistent::set('fecha', trim(htmlspecialchars($_POST['dateEvent'])));
            FormPersistent::set('hora', trim(htmlspecialchars($_POST['timeEvent'])));
            FormPersistent::set('tipo', trim(htmlspecialchars($_POST['tipo'])));
            FormPersistent::set('aforo', trim(htmlspecialchars($_POST['aforo'])));
        }
        App::get('router')->redirect('create-element');
    }

    public function show(int $id)
    {
        $sql = 'SELECT * FROM COMMENT WHERE ID_POST = :ID_POST AND REPLY = FALSE ORDER BY FECHA DESC';
        $comments = App::getRepository(CommentDB::class)->executeSql($sql, [':ID_POST' => $id]);
        $comments ?? [];

        $sql_reply = 'SELECT * FROM COMMENT WHERE ID_POST = :ID_POST AND REPLY = TRUE ORDER BY FECHA DESC';
        $replys = App::getRepository(CommentDB::class)->executeSql($sql_reply, [':ID_POST' => $id]);
        $replys ?? [];

        $evento = App::getRepository(PostDB::class)->find($id);
        $evento ?? null;
        if ($evento)
        {
            $relacionados = App::getRepository(PostDB::class)->findBy(
                ['TIPO' => $evento->getTipo(),
                'ID_USUARIO' => $evento->getIdUsuario()]);
        }
        $ultimo = FlashMessage::get('ultimo');
        $primero = FlashMessage::get('primero');
        $nuevo_comentario = FlashMessage::get('nuevo-comentario');
        $error_comentario = FlashMessage::get('error-comentario');
            
        Response::renderView(
            'blog-details',
            compact('evento', 'relacionados', 'ultimo', 'primero', 'nuevo_comentario', 'error_comentario', 'comments', 'replys')
        );
    }

    public function next()
    {
        $sql = "SELECT ID FROM POST WHERE FECHA_CREACION > :FECHA ORDER BY FECHA_CREACION ASC LIMIT 1";
        $rs_evento = App::getRepository(PostDB::class)->executeSqlGeneric($sql,[':FECHA' => $_POST['fecha'] . ' 23:59:59']);
        if (count($rs_evento) == 0)
        {
            header('Location:' . getenv('HTTP_REFERER'));
            FlashMessage::set('ultimo', 'This is the last event in the list');
        }
        else
        {
            $ID = $rs_evento[0]['ID'];
            App::get('router')->redirect('details/' . $ID);
        }
    }

    public function previous()
    {
        $sql = "SELECT ID FROM POST WHERE FECHA_CREACION < :FECHA ORDER BY FECHA_CREACION DESC LIMIT 1";
        $rs_evento = App::getRepository(PostDB::class)->executeSqlGeneric($sql,[':FECHA' => $_POST['fecha']]);
        if (count($rs_evento) == 0)
        {
            header('Location:' . getenv('HTTP_REFERER'));
            FlashMessage::set('primero', 'This is the first event in the list');
        }
        else
        {
            $ID = $rs_evento[0]['ID'];
            App::get('router')->redirect('details/' . $ID);
        }
    }

    public function userEvents(int $id)
    {
        FormPersistent::set('filtro_usuario', $id);

        App::get('router')->redirect('');
    }

    public function DeleteComment()
    {
        $pdo = App::getConnection();

        try
        {
            $se_puede_borrar = false;
            $es_reply = false;
            $pdo->beginTransaction();
            if (isset($_POST['idR']) && !empty($_POST['idR']))
            {
                App::getRepository(CommentDB::class)->delete($_POST['idR']);
                $se_puede_borrar = true;
                $es_reply = true;
            }
            else
            {
                $sql_reply = "SELECT COUNT(*) AS 'LISTA' FROM COMMENT WHERE ID_REPLY = :ID_REPLY AND REPLY = TRUE";
                $replys = App::getRepository(CommentDB::class)->executeSqlGeneric($sql_reply, [':ID_REPLY' => $_POST['idC']]);
                $replys ?? [];
                if ($replys[0]['LISTA'] == 0)
                {
                    $se_puede_borrar = true;
                    App::getRepository(CommentDB::class)->delete($_POST['idC']);
                }
                else
                {
                    FlashMessage::set(
                        'error-comentario', 'You can not delete comments that have been replicated');
                }
            }

            if ($se_puede_borrar)
            {
                $sql = 'UPDATE POST SET COMMENT_CANT = COMMENT_CANT - 1 WHERE ID = ' . $_POST['idP'];
                App::getRepository(PostDB::class)->executeSqlNotReturn($sql);

                $id = ($es_reply) ? $_POST['idR'] : $_POST['idC'];

                App::getLogger()->addWarning(
                    'Un comentario eliminado del usuario:' . $_POST['idU'] . ' con id:' . $id);

                FlashMessage::set(
                    'nuevo-comentario', 'Se ha eliminado el comentario');
            }

            $pdo->commit();
            
        }
        catch(PDOException $exception)
        {
            $pdo->rollBack();

            FlashMessage::set(
                'error-comentario', $exception->getMessage());
        }
        App::get('router')->redirect('details/' . $_POST['idP']);
    }

    //Borrar mas adelante
    public function pruebasParaExpansiones()
    {
        Response::renderView(
            'pruebas'
        );
    }
}