<?php

namespace dwes\app\controllers;

use dwes\app\repository\UsuarioDB;
use dwes\app\entity\Usuario;
use dwes\core\App;
use dwes\core\helpers\FlashMessage;
use dwes\core\helpers\FormPersistent;
use dwes\core\Response;
use dwes\core\Security;

class AuthController
{
    public function login()
    {
        $loginError = FlashMessage::get('login-error');

        Response::renderView(
            'login',
            compact(
                'loginError'
            )
        );
    }

    public function checkLogin()
    {
        if (!isset($_POST['email']) || !isset($_POST['password']) ||
            empty($_POST['email']) || empty($_POST['password'])) {
            FlashMessage::set('login-error', 'You must to introduce email and password');
            App::get('router')->redirect('login');
        }

        $usuarios = App::getRepository(UsuarioDB::class)->findBy(
            [
                'email' => $_POST['email'],
            ]
        );

        if (empty($usuarios) && Security::checkPassword($_POST['password'], $usuarios->getPassword()))
        {
            FlashMessage::set('login-error', 'Email or password was incorrect');
            App::get('router')->redirect('login');
        }

        $_SESSION['usuario'] = $usuarios[0]->getId();
        App::get('router')->redirect('my-elements');
    }

    public function logout()
    {
        $_SESSION['usuario'] = null;

        unset($_SESSION['usuario']);

        App::get('router')->redirect('');
    }

    public function unauthorized()
    {
        header(
            'HTTP/1.1 403 Forbidden',
            true,
            403);
        Response::renderView('403');
    }

    public function register()
    {
        $nameUser = FormPersistent::get('nameUser');
        $ScNameUser = FormPersistent::get('ScNameUser');
        $provincia = FormPersistent::get('provincia');
        $email = FormPersistent::get('email');
        $password = FormPersistent::get('password');

        $error = FlashMessage::get('error');
        $nuevoUsuario = FlashMessage::get('nuevo-usuario');

        $data = compact('error', 'nuevoUsuario', 'nameUser', 'ScNameUser', 'provincia', 'email', 'password');
        Response::renderView(
            'register',
            $data
        );
    }

    public function newRegister()
    {
        if (isset($_POST['nameUser']) && isset($_POST['email']) && isset($_POST['email2']) && isset($_POST['password'])
            && !empty($_POST['nameUser']) && !empty($_POST['email']) && !empty($_POST['email2']) && !empty($_POST['password'])) {
            $usuario = new Usuario();

            App::getConnection();

            $usuario->setName(trim(htmlspecialchars($_POST['nameUser'])));
            $usuario->setApellidos(trim(htmlspecialchars($_POST['ScNameUser'])));
            $usuario->setProvincia(trim(htmlspecialchars($_POST['provincia'])));
            $usuario->setEmail(trim(htmlspecialchars($_POST['email'])));
            $usuario->setPassword(trim(htmlspecialchars(Security::encrypt($_POST['password']))));
            $usuario->setCodigo('1234' . trim(htmlspecialchars($_POST['ScNameUser'])));
            $usuario->setRole('ROLE_USER');

            if ($_POST['email'] !== $_POST['email2']) {
                FlashMessage::set(
                    'error', 'Both input email must be equal'
                );
            } else {
                App::getRepository(UsuarioDB::class)->insert($usuario);
                App::getLogger()->addWarning(
                    'Nuevo usuario creado ' . $usuario->getName());
                FlashMessage::set('nuevo-usuario', 'User with name ' . $usuario->getName() . ' has created');
            }
        } else {
            FlashMessage::set('error', 'The inputs are empty or not defined');

            FormPersistent::set('nameUser', trim(htmlspecialchars($_POST['nameUser'])));
            FormPersistent::set('ScNameUser', trim(htmlspecialchars($_POST['ScNameUser'])));
            FormPersistent::set('provincia', trim(htmlspecialchars($_POST['provincia'])));
            FormPersistent::set('email', trim(htmlspecialchars($_POST['email'])));
            FormPersistent::set('password', trim(htmlspecialchars($_POST['password'])));
        }
        App::get('router')->redirect('register');
    }

    public function profile(int $id)
    {
        App::RenderView(
            'my-profile',
            compact('id')
        );
    }
}