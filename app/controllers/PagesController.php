<?php

namespace dwes\app\controllers;

use dwes\core\Response;
use dwes\core\App;
use dwes\app\repository\PostDB;
use dwes\core\helpers\FormPersistent;
use dwes\core\helpers\FlashMessage;

class PagesController
{
    public function about()
    {
        Response::renderView(
            'about'
        );
    }

    public function contact()
    {
        Response::renderView(
            'contact-us'
        );
    }

    public function gallery()
    {
        Response::renderView(
            'gallery'
        );
    }

    public function filtrar(int $page = null)
    {
        FlashMessage::set('filtros', 'Showing filter elements');
        $parametros = [];
        $sql = 'SELECT * FROM POST WHERE 1 = 1 ';
        $count_sql = "SELECT COUNT(*) AS 'LISTA' FROM POST WHERE 1 = 1 ";
        if (isset($_POST['fecha1']) && isset($_POST['fecha2']) && !empty($_POST['fecha1']) && !empty($_POST['fecha2']))
        {
            if ($_POST['fecha1'] > $_POST['fecha2'])
            {
                $fecha1 = $_POST['fecha2'];
                $fecha2 = $_POST['fecha1'];
            }
            else
            {
                $fecha1 = $_POST['fecha1'];
                $fecha2 = $_POST['fecha2'];
            }
            $parte_sql = ' AND FECHA BETWEEN :FECHA1 AND :FECHA2';
            $sql .= $parte_sql;
            $count_sql .= $parte_sql;
            $parametros[':FECHA1'] = trim(htmlspecialchars($fecha1 . ' 00:00:00'));
            $parametros[':FECHA2'] = trim(htmlspecialchars($fecha2 . ' 23:59:59'));
        }
        if (!empty($_POST['fecha1']) && isset($_POST['fecha1']) && empty($_POST['fecha2']))
        {
            $parte_sql = ' AND FECHA >= :FECHA1';
            $sql .= $parte_sql;
            $count_sql .= $parte_sql;
            $parametros[':FECHA1'] = trim(htmlspecialchars($_POST['fecha1']));
        }
        if (!empty($_POST['fecha2']) && isset($_POST['fecha2']) && empty($_POST['fecha1']))
        {
            $parte_sql = ' AND FECHA <= :FECHA2';
            $sql .= $parte_sql;
            $count_sql .= $parte_sql;
            $parametros[':FECHA2'] = trim(htmlspecialchars($_POST['fecha2']));
        }
        if (isset($_POST['tipo']) && !empty($_POST['tipo']))
        {
            $parte_sql = ' AND UPPER(TIPO) LIKE :TIPO';
            $sql .= $parte_sql;
            $count_sql .= $parte_sql;
            $parametros[':TIPO'] = trim(htmlspecialchars(strtoupper($_POST['tipo'])));
        }
        if (isset($_POST['texto']) && !empty($_POST['texto']))
        {
            $parte_sql = ' AND UPPER(TITULO) LIKE :TEXTO';
            $sql .= $parte_sql;
            $count_sql .= $parte_sql;
            $parametros[':TEXTO'] = "%".trim(htmlspecialchars(strtoupper($_POST['texto'])))."%";
        }

        FormPersistent::set('sql_filtros', $sql);
        FormPersistent::set('sql_count_filtros', $count_sql);
        FormPersistent::set('sql_count_filtros', $count_sql);
        $_SESSION['parametros'] = $parametros;
        FormPersistent::set('filtro_personalizado', 'Filtramos');

        App::get('router')->redirect('');
    }

    public function inicio(int $page = null)
    {
        $TAMANO_PAGINA = 6;
        $id_usuario = FormPersistent::get('filtro_usuario');
        $filtro_personalizado = FormPersistent::get('filtro_personalizado');

        if($page == null){
            $pagina = null;
        }
        else
        {
            $pagina = $page; 
        }
        if (!$pagina)
            {
            $inicio = 0;
            $pagina = 1;
        }
        else
        {
            $inicio = ($pagina - 1) * $TAMANO_PAGINA;
        }

        if($id_usuario)
        {
            $sql_count = "SELECT COUNT(*) AS 'LISTA' FROM POST WHERE ID_USUARIO = :ID";
            $num_total_registros = App::getRepository(PostDB::class)->executeSqlGeneric($sql_count, [':ID' => trim(htmlspecialchars($id_usuario))]);
            $num_total_registros = intval($num_total_registros[0]['LISTA']);
            
            $TOTAL_PAGINAS = ceil($num_total_registros / $TAMANO_PAGINA);
            $sql = 'SELECT * FROM POST WHERE ID_USUARIO = :ID ORDER BY FECHA DESC LIMIT ' . $inicio . ',' . $TAMANO_PAGINA;
            $eventos = App::getRepository(PostDB::class)->executeSql($sql, [':ID' => trim(htmlspecialchars($id_usuario))]);
        }
        else if ($filtro_personalizado)
        {
            $parametros = $_SESSION['parametros'];
            unset($_SESSION['parametros']);
            $sql = FormPersistent::get('sql_filtros');
            $count_sql = FormPersistent::get('sql_count_filtros');
            $num_total_registros = App::getRepository(PostDB::class)->executeSqlGeneric($count_sql, $parametros);
            $num_total_registros = intval($num_total_registros[0]['LISTA']);

            $TOTAL_PAGINAS = ceil($num_total_registros / $TAMANO_PAGINA);
            $sql = $sql . ' ORDER BY FECHA DESC LIMIT ' . $inicio . ',' . $TAMANO_PAGINA;
            $eventos = App::getRepository(PostDB::class)->executeSql($sql, $parametros);
        }
        else
        {
            $sql_count = "SELECT COUNT(*) AS 'LISTA' FROM POST ORDER BY FECHA DESC";
            $num_total_registros = App::getRepository(PostDB::class)->executeSqlGeneric($sql_count);
            $num_total_registros = intval($num_total_registros[0]['LISTA']);
            
            $TOTAL_PAGINAS = ceil($num_total_registros / $TAMANO_PAGINA);
            $sql = 'SELECT * FROM POST ORDER BY FECHA DESC LIMIT ' . $inicio . ',' . $TAMANO_PAGINA;
            $eventos = App::getRepository(PostDB::class)->executeSql($sql);
        }
        $filter_aply = FlashMessage::get('filtros');
 
        $eventos ?? [];
        Response::renderView(
            'index',
            compact('eventos', 'TOTAL_PAGINAS', 'pagina', 'filter_aply')
        );
    }
}