<?php


use Monolog\Logger;

return [
    'database' => [
        'name' => 'acme',
        'username' => 'acmeuser',
        'password' => 'm2at5a7n64do1m8o',
        'connection' => 'mysql:host=dwes.local',
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true
        ]
    ],
    'logger' => [
        'name' => 'log-dwes',
        'file' => 'logs/dwes.log',
        'level' => Logger::WARNING
    ],
    'security' => array(
        'roles' => array(
            'ROLE_ADMIN'=>3,
            'ROLE_USER'=>2,
            'ROLE_ANONIMO'=>1
        )
    ),
    'mailer' => [
        'smtp' => 'smtp.gmail.com',
        'correo' => 'saezcasesd@gmail.com',
        'password' => 'm2at5a7n64do1m8o',
        'encrypt' => 'ssl',
        'port' => 465
    ]
];