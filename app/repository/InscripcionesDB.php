<?php

namespace dwes\app\repository;

use dwes\core\database\QueryBuilder;
use dwes\app\entity\Inscripciones;

class InscripcionesDB extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct('INSCRIPCIONES', Inscripciones::class);
    }
}