<?php

namespace dwes\app\repository;

use dwes\core\database\QueryBuilder;
use dwes\app\entity\Post;

class PostDB extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(
            'POST',
            Post::class);
    }
}