<?php

namespace dwes\app\repository;

use dwes\core\database\QueryBuilder;
use dwes\app\entity\Message;

class MessageDB extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct('MESSAGE', Message::class);
    }
}