<?php

namespace dwes\app\repository;

use dwes\core\database\QueryBuilder;
use dwes\app\entity\Comment;

class CommentDB extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct('COMMENT', Comment::class);
    }
}