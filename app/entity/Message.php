<?php

namespace dwes\app\entity;

use dwes\app\entity\IEntity;
use dwes\app\repository\UsuarioDB;
use dwes\core\App;

class Message implements IEntity
{
    /**
     * @var int
     */
    private $ID;
    /**
     * @var int
     */
    private $REMITENTE;
    /**
     * @var int
     */
    private $DESTINATARIO;
    /**
     * @var string
     */
    private $MENSAJE;
    /**
     * @var string
     */
    private $FECHA;

    /**
     * @param int $id
     * @return mixed
     */
    public static function getUsuario(int $id)
    {
        $usuario = App::getRepository(UsuarioDB::class)->find($id);
        return $usuario;
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->ID;
    }

    /**
     * @return int
     */
    public function getREMITENTE(): int
    {
        return $this->REMITENTE;
    }

    /**
     * @param int $REMITENTE
     * @return Message
     */
    public function setREMITENTE(int $REMITENTE): Message
    {
        $this->REMITENTE = $REMITENTE;
        return $this;
    }

    /**
     * @return int
     */
    public function getDESTINATARIO(): int
    {
        return $this->DESTINATARIO;
    }

    /**
     * @param int $DESTINATARIO
     * @return Message
     */
    public function setDESTINATARIO(int $DESTINATARIO): Message
    {
        $this->DESTINATARIO = $DESTINATARIO;
        return $this;
    }

    /**
     * @return string
     */
    public function getMENSAJE(): string
    {
        return $this->MENSAJE;
    }

    /**
     * @param string $MENSAJE
     * @return Message
     */
    public function setMENSAJE(string $MENSAJE): Message
    {
        $this->MENSAJE = $MENSAJE;
        return $this;
    }

    /**
     * @return string
     */
    public function getFECHA(): string
    {
        return $this->FECHA;
    }

    public function toArray()
    {
        return [
            'ID' => $this->ID,
            'REMITENTE' => $this->REMITENTE,
            'DESTINATARIO' => $this->DESTINATARIO,
            'MENSAJE' => $this->MENSAJE,
            'FECHA' => $this->FECHA
        ];
    }
}