<?php

namespace dwes\app\entity;

use dwes\app\utils\UploadFile;
use dwes\core\App;
use dwes\app\repository\UsuarioDB;

class Post implements IEntity
{
    use UploadFile;

    public const RUTA_IMGS = '/images/gallery/';

    /**
     * @var int
     */
    private $ID;
    /**
     * @var int
     */
    private $ID_USUARIO;
    /**
     * @var int
     */
    private $COMMENT_CANT = 0;
    /**
     * @var string
     */
    private $TITULO;
    /**
     * @var string
     */
    private $IMAGEN = 'defaultEvent.png';
    /**
     * @var string
     */
    private $TEXTO;
    /**
     * @var string
     */
    private $TIPO;
    /**
     * @var int
     */
    private $AFORO = 10;
    /**
     * @var DateTime
     */
    private $FECHA;
    /**
     * @var DateTime
     */
    private $FECHA_CREACION;
    /**
     * @var bool
     */
    private $VISIBLE = true;

    /**
     * @return bool
     */
    public function isVISIBLE(): bool
    {
        return $this->VISIBLE;
    }

    /**
     * @param bool $VISIBLE
     * @return Post
     */
    public function setVISIBLE(bool $VISIBLE): Post
    {
        $this->VISIBLE = $VISIBLE;
        return $this;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function getUsuario(int $id)
    {
        $usuario = App::getRepository(UsuarioDB::class)->find($id);
        return $usuario;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->ID;
    }

    /**
     * @return int
     */
    public function getIdUsuario(): int
    {
        return $this->ID_USUARIO;
    }

    /**
     * @param int $ID_USUARIO
     * @return Imagen
     */
    public function setIdUsuario(int $ID_USUARIO): Post
    {
        $this->ID_USUARIO = $ID_USUARIO;
        return $this;
    }

    /**
     * @return int
     */
    public function getCommentCant(): int
    {
        return $this->COMMENT_CANT;
    }

    /**
     * @param int $COMMENT_CANT
     * @return Post
     */
    public function setCommentCant(int $COMMENT_CANT): Post
    {
        $this->COMMENT_CANT = $COMMENT_CANT;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo(): string
    {
        return $this->TITULO;
    }

    /**
     * @param string $TITULO
     * @return Post
     */
    public function setTitulo(string $TITULO): Post
    {
        $this->TITULO = $TITULO;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagen(): string
    {
        return $this->IMAGEN;
    }

    /**
     * @param string $IMAGEN
     * @return Imagen
     */
    public function setImagen(string $IMAGEN): Post
    {
        $this->IMAGEN = $IMAGEN;
        return $this;
    }

    /**
     * @return string
     */
    public function getTexto(): string
    {
        return $this->TEXTO;
    }

    /**
     * @param string $TEXTO
     * @return Post
     */
    public function setTexto(string $TEXTO): Post
    {
        $this->TEXTO = $TEXTO;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipo(): string
    {
        return $this->TIPO;
    }

    /**
     * @param string $TIPO
     * @return Post
     */
    public function setTipo(string $TIPO): Post
    {
        $this->TIPO = $TIPO;
        return $this;
    }

    /**
     * @return int
     */
    public function getAFORO(): int
    {
        return $this->AFORO;
    }

    /**
     * @param int $AFORO
     * @return Post
     */
    public function setAFORO(int $AFORO): Post
    {
        $this->AFORO = $AFORO;
        return $this;
    }

    /**
     * @return string
     */
    public function getFecha(): string
    {
        return $this->FECHA;
    }

    /**
     * @param string $FECHA
     * @return Post
     */
    public function setFecha(string $FECHA): Post
    {
        $this->FECHA = $FECHA;
        return $this;
    }

    /**
     * @return string
     */
    public function getFechaCreacion(): string
    {
        return $this->FECHA_CREACION;
    }

    /**
     * @return string
     */
    public function getUrlImage()
    {
        return self::RUTA_IMGS . $this->getImagen();
    }

    public function toArray()
    {
        return [
            'ID_USUARIO' => $this->ID_USUARIO,
            'COMMENT_CANT' => $this->COMMENT_CANT,
            'TITULO' => $this->TITULO,
            'IMAGEN' => $this->IMAGEN,
            'TEXTO' => $this->TEXTO,
            'TIPO' => $this->TIPO,
            'AFORO' => $this->AFORO,
            'FECHA' => $this->FECHA,
            'FECHA_CREACION' => $this->FECHA_CREACION,
            'VISIBLE' => $this->VISIBLE
        ];
    }
}