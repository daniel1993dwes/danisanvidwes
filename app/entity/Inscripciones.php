<?php

namespace dwes\app\entity;

use dwes\app\entity\IEntity;
use dwes\app\repository\PostDB;
use dwes\app\repository\UsuarioDB;
use dwes\core\App;

class Inscripciones implements IEntity
{
    /**
     * @var int
     */
    private $ID;
    /**
     * @var int
     */
    private $ID_USUARIO;
    /**
     * @var int
     */
    private $ID_EVENTO;
    /**
     * @var string
     */
    private $EMAIL;
    /**
     * @var string
     */
    private $NOMBRE;
    /**
     * @var string
     */
    private $APELLIDOS;
    /**
     * @var string
     */
    private $FECHA;

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->ID;
    }

    /**
     * @return int
     */
    public function getIDUSUARIO(): int
    {
        return $this->ID_USUARIO;
    }

    /**
     * @param int $ID_USUARIO
     * @return Inscripciones
     */
    public function setIDUSUARIO(int $ID_USUARIO): Inscripciones
    {
        $this->ID_USUARIO = $ID_USUARIO;
        return $this;
    }

    /**
     * @return int
     */
    public function getIDEVENTO(): int
    {
        return $this->ID_EVENTO;
    }

    /**
     * @param int $ID_EVENTO
     * @return Inscripciones
     */
    public function setIDEVENTO(int $ID_EVENTO): Inscripciones
    {
        $this->ID_EVENTO = $ID_EVENTO;
        return $this;
    }

    /**
     * @return string
     */
    public function getEMAIL(): string
    {
        return $this->EMAIL;
    }

    /**
     * @param string $EMAIL
     * @return Inscripciones
     */
    public function setEMAIL(string $EMAIL): Inscripciones
    {
        $this->EMAIL = $EMAIL;
        return $this;
    }

    /**
     * @return string
     */
    public function getNOMBRE(): string
    {
        return $this->NOMBRE;
    }

    /**
     * @param string $NOMBRE
     * @return Inscripciones
     */
    public function setNOMBRE(string $NOMBRE): Inscripciones
    {
        $this->NOMBRE = $NOMBRE;
        return $this;
    }

    /**
     * @return string
     */
    public function getAPELLIDOS(): string
    {
        return $this->APELLIDOS;
    }

    /**
     * @param string $APELLIDOS
     * @return Inscripciones
     */
    public function setAPELLIDOS(string $APELLIDOS): Inscripciones
    {
        $this->APELLIDOS = $APELLIDOS;
        return $this;
    }

    /**
     * @return string
     */
    public function getFECHA(): string
    {
        return $this->FECHA;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function getUsuario(int $id)
    {
        $usuario = App::getRepository(UsuarioDB::class)->find($id);
        return $usuario;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function getPost(int $id)
    {
        $event = App::getRepository(PostDB::class)->find($id);
        return $event;
    }

    public function toArray()
    {
        return [
            'ID' => $this->ID,
            'ID_EVENTO' => $this->ID_EVENTO,
            'ID_USUARIO' => $this->ID_USUARIO,
            'EMAIL' => $this->EMAIL,
            'NOMBRE' => $this->NOMBRE,
            'APELLIDOS' => $this->APELLIDOS,
            'FECHA' => $this->FECHA
        ];
    }
}