<?php

namespace dwes\app\entity;

use dwes\app\entity\Post;
use dwes\app\entity\Usuario;
use dwes\app\entity\IEntity;
use dwes\app\repository\UsuarioDB;
use dwes\app\repository\CommentDB;
use dwes\core\App;

class Comment implements IEntity
{
    /**
     * @var int
     */
    private $ID;
    /**
     * @var int
     */
    private $ID_POST;
    /**
     * @var int
     */
    private $ID_USER;
    /**
     * @var string
     */
    private $TEXT;
    /**
     * @var bool
     */
    private $REPLY = false;
    /**
     * @var int
     */
    private $ID_REPLY = -1;
    /**
     * @var DateTime
     */
    private $FECHA;

    /**
     * @return string
     */
    public function getFECHA(): string
    {
        return $this->FECHA;
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->ID;
    }

    /**
     * @return int
     */
    public function getIDPOST(): int
    {
        return $this->ID_POST;
    }

    /**
     * @param int $ID_POST
     * @return Comment
     */
    public function setIDPOST(int $ID_POST): Comment
    {
        $this->ID_POST = $ID_POST;
        return $this;
    }

    /**
     * @return int
     */
    public function getIDUSER(): int
    {
        return $this->ID_USER;
    }

    /**
     * @param int $ID_USER
     * @return Comment
     */
    public function setIDUSER(int $ID_USER): Comment
    {
        $this->ID_USER = $ID_USER;
        return $this;
    }

    /**
     * @return string
     */
    public function getTEXT(): string
    {
        return $this->TEXT;
    }

    /**
     * @param string $TEXT
     * @return Comment
     */
    public function setTEXT(string $TEXT): Comment
    {
        $this->TEXT = $TEXT;
        return $this;
    }

    /**
     * @return bool
     */
    public function getREPLY(): bool
    {
        return $this->REPLY;
    }

    /**
     * @param bool $REPLY
     * @return Comment
     */
    public function setREPLY(bool $REPLY): Comment
    {
        $this->REPLY = $REPLY;
        return $this;
    }

    /**
     * @return int
     */
    public function getREPLYID(): int
    {
        return $this->ID_REPLY;
    }

    /**
     * @param int $REPLY
     * @return Comment
     */
    public function setREPLYID(int $ID_REPLY): Comment
    {
        $this->ID_REPLY = $ID_REPLY;
        return $this;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function getUsuario(int $id)
    {
        $usuario = App::getRepository(UsuarioDB::class)->find($id);
        return $usuario;
    }

    public function toArray()
    {
        return [
            'ID' => $this->ID,
            'ID_POST' => $this->ID_POST,
            'ID_USER' => $this->ID_USER,
            'ID_REPLY' => $this->ID_REPLY,
            'TEXT' => $this->TEXT,
            'REPLY' => $this->REPLY
        ];
    }
}