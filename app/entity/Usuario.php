<?php

namespace dwes\app\entity;

use dwes\app\utils\UploadFile;

class Usuario implements IEntity
{
    use UploadFile;

    public const RUTA_IMGS = '/images/users/';

    /**
     * @var int
     */
    private $ID;
    /**
     * @var string
     */
    private $NAME;
    /**
     * @var string
     */
    private $APELLIDOS;
    /**
     * @var string
     */
    private $PROVINCIA;
    /**
     * @var string
     */
    private $EMAIL;
    /**
     * @var string
     */
    private $CODIGO;
    /**
     * @var string
     */
    private $PASSWORD;
    /**
     * @var string
     */
    private $AVATAR = 'userDefault.jpg';
    /**
     * @var string
     */
    private $ROLE = 'ROLE_ANONIMO';
    /**
     * @var string
     */
    private $FECHA;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->ID;
    }

    /**
     * @param string $NAME
     * @return Usuario
     */
    public function setName(string $NAME): Usuario
    {
        $this->NAME = $NAME;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->NAME;
    }

    /**
     * @param string $APELLIDOS
     * @return Usuario
     */
    public function setApellidos(string $APELLIDOS): Usuario
    {
        $this->APELLIDOS = $APELLIDOS;
        return $this;
    }

    /**
     * @return string
     */
    public function getApellidos(): string
    {
        return $this->APELLIDOS;
    }

    /**
     * @param string $PROVINCIA
     * @return Usuario
     */
    public function setProvincia(string $PROVINCIA): Usuario
    {
        $this->PROVINCIA = $PROVINCIA;
        return $this;
    }

    /**
     * @return string
     */
    public function getProvincia(): string
    {
        return $this->PROVINCIA;
    }

    /**
     * @param string $EMAIL
     * @return Usuario
     */
    public function setEmail(string $EMAIL): Usuario
    {
        $this->EMAIL = $EMAIL;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->EMAIL;
    }

    /**
     * @return string
     */
    public function getCodigo(): string
    {
        return $this->CODIGO;
    }

    /**
     * @param string $CODIGO
     * @return Usuario
     */
    public function setCodigo(string $CODIGO): Usuario
    {
        $this->CODIGO = $CODIGO;
        return $this;
    }

    /**
     * @param string $PASSWORD
     * @return Usuario
     */
    public function setPassword(string $PASSWORD): Usuario
    {
        $this->PASSWORD = $PASSWORD;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->PASSWORD;
    }

    /**
     * @param string $AVATAR
     * @return Usuario
     */
    public function setAvatar(string $AVATAR): Usuario
    {
        $this->AVATAR = $AVATAR;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->AVATAR;
    }

    /**
     * @param string $ROLE
     * @return Usuario
     */
    public function setRole(string $ROLE): Usuario
    {
        $this->ROLE = $ROLE;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->ROLE;
    }

    /**
     * @return string
     */
    public function getFecha(): string
    {
        return $this->FECHA;
    }


    public function getUrlAvatar()
    {
        return self::RUTA_IMGS . $this->getAvatar();
    }

    public function toArray()
    {
        return [
            'ID' => $this->ID,
            'NAME' => $this->NAME,
            'APELLIDOS' => $this->APELLIDOS,
            'PROVINCIA' => $this->PROVINCIA,
            'EMAIL' => $this->EMAIL,
            'CODIGO' => $this->CODIGO,
            'PASSWORD' => $this->PASSWORD,
            'AVATAR' => $this->AVATAR,
            'ROLE' => $this->ROLE,
            'FECHA' => $this->FECHA
        ];
    }
}