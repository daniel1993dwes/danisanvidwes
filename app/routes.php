<?php

use dwes\core\App;

$router = App::get('router');
$router->get('', 'PagesController@inicio');
$router->get(':pagina', 'PagesController@inicio');
$router->get('about', 'PagesController@about');
$router->get('register', 'AuthController@register');
$router->post('new-register', 'AuthController@newRegister');
$router->get('login', 'AuthController@login');
$router->post('check-login', 'AuthController@checkLogin');
$router->get('logout', 'AuthController@logout', 'ROLE_USER');
$router->get('create-element', 'PostController@crearElemento', 'ROLE_USER');
$router->post('element/new', 'PostController@nuevoElemento', 'ROLE_USER');
$router->get('contact', 'PagesController@contact');
$router->post('element/filter', 'PagesController@filtrar');
$router->get('details/:id', 'PostController@show');
$router->post('next', 'PostController@next');
$router->post('previous', 'PostController@previous');
$router->get('user/events/:id', 'PostController@userEvents');
$router->post('comments', 'PostController@AddComment', 'ROLE_USER');
$router->get('get-inscripcion/:id', 'PostController@GetInscripcion', 'ROLE_USER');
$router->post('new/inscripcion', 'PostController@NewInscripcion', 'ROLE_USER');
$router->post('delete/comment', 'PostController@DeleteComment', 'ROLE_USER');
$router->get('sent/:id', 'MessageController@Sent', 'ROLE_USER');
$router->post('new/message', 'MessageController@NewMessage', 'ROLE_USER');
$router->post('delete/message', 'MessageController@DeleteMessage', 'ROLE_USER');
$router->get('my-messages', 'MessageController@Messages','ROLE_USER');
$router->post('filter-user', 'UsuarioController@FindUser', 'ROLE_ADMIN');
$router->get('users-list', 'UsuarioController@GetUsuarios', 'ROLE_ADMIN');
$router->get('delete/user/:id', 'UsuarioController@DeleteUser', 'ROLE_ADMIN');
$router->post('change-role', 'UsuarioController@ChangeRole', 'ROLE_ADMIN');
$router->get('my-elements', 'PostController@MyElements', 'ROLE_USER');
$router->post('filter-elements', 'PostController@filtrar', 'ROLE_USER');
$router->get('delete-element/:id', 'PostController@DeleteEvent', 'ROLE_USER');
$router->get('modify-element/:id', 'PostController@ChargeModifyForm', 'ROLE_USER');
$router->post('modify', 'PostController@ModifyEvent', 'ROLE_USER');
$router->get('my-profile', 'UsuarioController@ChargeUserProfile', 'ROLE_USER');
$router->post('change-password', 'UsuarioController@ChangePassword', 'ROLE_USER');
$router->post('change-avatar', 'UsuarioController@ChangeAvatar', 'ROLE_USER');
$router->get('idioma/:idioma', 'InternacionalizationController@cambiaIdioma');

// PRUEBAS PARA EXPANDIR LA APP
$router->get('pruebas', 'PostController@pruebasParaExpansiones', 'ROLE_ADMIN');