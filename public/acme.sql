-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 07-01-2019 a las 18:46:30
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `acme`
--
CREATE DATABASE IF NOT EXISTS `acme` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `acme`;

-- --------------------------------------------------------
CREATE USER 'acmeuser'@'localhost' IDENTIFIED BY 'm2at5a7n64do1m8o'; 
GRANT USAGE ON *.* TO 'acmeuser'@'localhost' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT SELECT, INSERT, UPDATE, DELETE ON acme.* TO 'acmeuser'@'localhost';
--
-- Estructura de tabla para la tabla `COMMENT`
--

DROP TABLE IF EXISTS `COMMENT`;
CREATE TABLE `COMMENT` (
  `ID` int(11) NOT NULL,
  `ID_POST` int(11) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `TEXT` text COLLATE utf8_spanish_ci NOT NULL,
  `REPLY` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `ID_REPLY` int(11) DEFAULT NULL,
  `FECHA` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `INSCRIPCIONES`
--

DROP TABLE IF EXISTS `INSCRIPCIONES`;
CREATE TABLE `INSCRIPCIONES` (
  `ID` int(11) NOT NULL,
  `ID_EVENTO` int(11) NOT NULL,
  `ID_USUARIO` int(11) NOT NULL,
  `EMAIL` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `NOMBRE` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `APELLIDOS` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `FECHA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `INSCRIPCIONES`
--

INSERT INTO `INSCRIPCIONES` (`ID`, `ID_EVENTO`, `ID_USUARIO`, `EMAIL`, `NOMBRE`, `APELLIDOS`, `FECHA`) VALUES
(12, 38, 4, 'dani_sc1993@hotmail.com', 'Daniel', 'Sáez Cases', '2019-01-07 17:45:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MESSAGE`
--

DROP TABLE IF EXISTS `MESSAGE`;
CREATE TABLE `MESSAGE` (
  `ID` int(11) NOT NULL,
  `REMITENTE` int(11) NOT NULL,
  `DESTINATARIO` int(11) NOT NULL,
  `MENSAJE` mediumtext COLLATE utf8_spanish_ci NOT NULL,
  `FECHA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `MESSAGE`
--

INSERT INTO `MESSAGE` (`ID`, `REMITENTE`, `DESTINATARIO`, `MENSAJE`, `FECHA`) VALUES
(1, 4, 4, 'HOLA QUE TAL ESTAS', '2019-01-07 17:35:51'),
(2, 4, 4, 'Buenas que tal?', '2019-01-07 17:40:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `POST`
--

DROP TABLE IF EXISTS `POST`;
CREATE TABLE `POST` (
  `ID` int(11) NOT NULL,
  `ID_USUARIO` int(11) NOT NULL DEFAULT '1',
  `COMMENT_CANT` int(11) NOT NULL DEFAULT '0',
  `TITULO` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `IMAGEN` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `TEXTO` mediumtext COLLATE utf8_spanish_ci NOT NULL,
  `TIPO` varchar(1000) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'EVENTO GENERICO',
  `AFORO` int(11) DEFAULT NULL,
  `FECHA` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `FECHA_CREACION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `VISIBLE` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `POST`
--

INSERT INTO `POST` (`ID`, `ID_USUARIO`, `COMMENT_CANT`, `TITULO`, `IMAGEN`, `TEXTO`, `TIPO`, `AFORO`, `FECHA`, `FECHA_CREACION`, `VISIBLE`) VALUES
(34, 4, 0, 'Comarcon conferencia sobre Japón', 'logo CCON18.png', '&lt;p&gt;La Directora General de la UNESCO, Irina Bokova y el ministro japonés de Educación, Deportes, Ciencia y Tecnología, Hakubun Shimomura, inauguraron hoy en Aichi-Nagoya (Japón) la Conferencia Mundial de Educación para el Desarrollo Sostenible (EDS). La ceremonia inaugural se celebró en presencia de Su Alteza Imperial el príncipe heredero del Japón, tras varias reuniones previas de los participantes celebradas la semana pasada en Okayama. \r\n“Para lograr el desarrollo sostenible, no bastan la tecnología, los reglamentos y los incentivos financieros”, declaró Bokova. “Debemos modificar también nuestro modo de pensar y de actuar como individuos y como sociedad, y ese es precisamente el objetivo de la educación para el desarrollo sostenible”, añadió. &lt;/p&gt;\r\n&lt;p&gt;\r\nSu Alteza Imperial el príncipe heredero del Japón habló también de la necesidad de que la educación figure en la vanguardia del cambio: “Para alcanzar el desarrollo sostenible, cada uno de nosotros debe reconocer que existe en relación a todos los demás habitantes del planeta y a las generaciones futuras, así como a su propio medio ambiente natural. Debemos estudiar los problemas mundiales con una profunda perspectiva internacional y yo estoy seguro de que la educación es la base para ello”.&lt;/p&gt;\r\n\r\n&lt;p&gt;\r\n“Esta conferencia es muy importante, porque podremos hacer balance del último decenio y debatir sobre el modo de seguir promoviendo la EDS. Espero que lo que aquí aprendamos permita acelerar la promoción de la EDS hoy y en el futuro y modifique la educación en todos los lugares del mundo”, dijo por su parte Hakubun Shimomura.&lt;/p&gt;\r\n\r\n&lt;p&gt;El ministro nipón anunció además la creación del Premio UNESCO-Japón de Educación para el Desarrollo Sostenible, cuyo objetivo será promover el programa de acción global para la EDS. El galardón, dotado con 50.000 dólares, recompensará durante los próximos cinco años tres trabajos individuales o colectivos que aporten contribuciones importantes a la promoción de una o varias de las cinco prioridades señaladas en el plan de acción.&lt;/p&gt;', 'EVENTO VIDEOJUEGOS', 22, '2019-01-17 21:30:00', '2019-01-07 17:09:39', 1),
(35, 4, 0, 'Cosplay en la sede de la asociacion', 'cabecera.jpg', 'Murcia se Remanga, el X Salón del Manga de Murcia, es uno de los eventos con más solera de España. En esta edición, varios miembros del equipo de CoolJapan.es se trasladan a Murcia para participar en las conferencias y en la recepción del invitado al evento, el Sr. Tokuyuki Matsutake.\r\n\r\nComo otras ediciones, en esta el despliegue de CoolJapan.es será muy amplio y por ello os dejamos esta guía para que podáis seguir nuestra colaboración con el evento:\r\n\r\n&lt;h3&gt;Cooljapan.es en el X Salón del Manga de Murcia&lt;/h3&gt;\r\n&lt;h4&gt;EL ANIME EN LOS VIDEOJUEGOS DE NAMCO&lt;/h4&gt;\r\n&lt;ul&gt;\r\n&lt;li&gt;Andrés Domenech&lt;/li&gt;\r\n&lt;li&gt;Domingo 25 a las 16h. Sala de proyecciones&lt;/li&gt;\r\n&lt;/ul&gt;\r\nAndrés Domenech, doctor especializado en videojuegos por la Facultad de Bellas Artes de la Universidad de Granada, nos hablará sobre el importante papel del manga y el anime en los videojuegos de Namco y la actual Bandai Namco a lo largo de los años, habiendo apostado por franquicias originales y por otras basadas en series de éxito como One Piece o Dragon Ball.', 'EVENTO COSPLAY', 30, '2019-01-25 01:25:00', '2019-01-07 17:14:35', 1),
(36, 4, 0, 'Charla sobre feminismo en la actualidad cultural', '2015-10-30.jpg', '&lt;p&gt;Ayer fue el día de mi conferencia en la XXI edición del Salón del Manga de Barcelona, y la verdad es que estuvo muy bien, espero que los asistentes opinen lo mismo. Tras una introducción del amiguete Oriol Estrada, que habló de de los samuráis dentro del mundo del manga y el cine, pudimos dedicar hora y media a repasar la historia de los samuráis desde sus orígenes hasta sus últimos días y la imagen que de ellos nos ha quedado en la actualidad. Seguimos más o menos el guión en el que se basa el libro que publicaré el año próximo, que también apareció brevemente en la conferencia y del que pronto podré dar más noticias.&lt;/p&gt;\r\n\r\n&lt;p&gt;Quiero dar las gracias a todos los asistentes, a la gente encargada de las tareas técnicas y de organización, a Oriol y todos en Espai Daruma por haberme invitado, y toda la gente con la que estuve hablando a lo largo del día de ayer.&lt;/p&gt;', 'EVENTO GENERICO', 30, '2019-01-30 20:10:00', '2019-01-07 17:17:11', 1),
(37, 4, 0, 'Reunion sobre las cuentas de la Asoc', 'curso-contabilidad-esacan-768x506.jpg', 'Cualquier modificación en los estatutos (cambios en la denominación, domicilio, fines y actividades, ámbito territorial, etc.).\r\nCambios en las personas titulares de los órganos de representación (Junta Directiva).\r\nLa apertura, cambio y cierre de delegaciones o establecimientos.\r\nLa declaración y la revocación de la condición de utilidad pública.\r\nLas asociaciones que constituyen o integran federaciones, confederaciones o uniones de asociaciones.\r\nLa incorporación y separación de asociaciones a una federación, confederación o unión de asociaciones o a entidades internacionales.\r\nLa suspensión, disolución o baja de la asociación y sus causas.\r\nLa apertura y el cierre de una delegación en España de asociaciones extranjera.\r\nEn los siguientes enlaces podéis encontrar un directorio de los Registros de Asociaciones de las Comunidades Autónomas y del Registro Nacional de Asociaciones:', 'EVENTO GENERICO', 1000, '2019-01-19 00:40:00', '2019-01-07 17:20:59', 1),
(38, 4, 0, 'Debate podcast para iniciar la temporada', 'Untitled-71.jpg', 'Quieres montar un podcast y no tienes ni pajolera idea de cómo hacerlo, ¿verdad? Aunque pueda parecer una tarea ardua y complicada, grabar un programa es más simple de lo que parece, hasta tal punto que puede salirte gratis (si cuentas con los elementos de serie, que por lo general se tienen ya en cualquier hogar).\r\n\r\nEn el día de hoy vamos a enseñarte a cómo hacer tu propio podcast de forma sencilla.\r\n\r\nLa idea y los colaboradores\r\nEstas dos cosas son las esenciales a la hora de montar tu propio programa: tener una buena idea y colaboradores que se amolden a la misma. Si quieres hacer un podcast de cine, lógicamente tendrás que contar con gente que esté puesta en el séptimo arte. “Eso ya lo sé”, me podrás decir, pero es que no se trata sólo de saber, sino también de que estas personas tengan disponibilidad horaria y un micrófono en su casa para grabar. La cosa se complica, ¿verdad?\r\n\r\nTraza bien tu proyecto y dedícale unos cuantos días –o semanas- para que funcione correctamente. Esto es como querer hacerte una casa y no tener ni los planos de la nueva', 'EVENTO GENERICO', 99, '2019-02-15 20:20:00', '2019-01-07 17:22:56', 1),
(39, 4, 0, 'Proyeccion película Totoro', 'proyeccion-la-pelicula-mi-vecino-totoro-concierto-sinfonico-1024x768.jpg', '&lt;p&gt;Totoro es uno de los personajes más queridos y emblemáticos del mundo animado, creado por el estudio cinematográfico japonés Ghibli. Es tan popular que verás su imagen retratada en decenas de productos: mochilas, vestidos, tenis, relojes, fundas para celular y hasta utensilios de cocina. Si aún no has visto la película de la que se ha tomado este curioso personaje, ésta es tu oportunidad.&lt;/p&gt;\r\n\r\n&lt;p&gt;Dirigida por el maestro de la animación japonesa y cofundador del Studio Ghibli, Hayao Miyazaki, Mi vecino Totoro se convirtió en un clásico infantil, el cual será proyectado en CDMX con subtítulos en español. Podrás disfrutar de la música original de Joe Hisaishi, interpretada por la Orquesta Filarmónica Metropolitana. Para complacer a los fans de hueso colorado, los temas serán interpretados en japonés, su idioma original.&lt;/p&gt;', 'EVENTO PROYECCIÓN DE PELÍCULA', 15, '2019-01-24 02:25:00', '2019-01-07 17:25:53', 1),
(40, 4, 0, 'Película La voz del silencio', 'Koe-no-Katachi-Una-Voz-Silenciosa.jpg', '&lt;p&gt;“Koe no Katachi” está basada en el reconocido manga de Yoshitoki Oima el cuál recibió el premio a “Mejor Manga Nuevo” en el 2008 y fue apoyado por la Federación Japonesa de Sordos. En el 2015 fue uno de los nominados para el Premio Cultural Osamu Tezuka y recibió el galardón de “Nuevos Creadores”. En 2016 fue nominado en Estados Unidos como Mejor Material Asiático en los Premios Eisner.\r\n\r\n“Koe no Katachi” fue la ganadora como Excelente Animación del Año por la Japan Academy, Anime del Año por el Tokyo Anime Award Festival y al Premio de la Excelencia por el Japan Media Arts Festival.&lt;/p&gt;', 'EVENTO PROYECCIÓN DE PELÍCULA', 15, '2019-01-24 21:30:00', '2019-01-07 17:29:06', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SUBSCRIPCION`
--

DROP TABLE IF EXISTS `SUBSCRIPCION`;
CREATE TABLE `SUBSCRIPCION` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `ID_USUARIO` int(11) NOT NULL,
  `ID_TIPO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `USUARIO`
--

DROP TABLE IF EXISTS `USUARIO`;
CREATE TABLE `USUARIO` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `APELLIDOS` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `PROVINCIA` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `EMAIL` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `CODIGO` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PASSWORD` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `AVATAR` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ROLE` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'ROLE_ADMIN',
  `FECHA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `USUARIO`
--

INSERT INTO `USUARIO` (`ID`, `NAME`, `APELLIDOS`, `PROVINCIA`, `EMAIL`, `CODIGO`, `PASSWORD`, `AVATAR`, `ROLE`, `FECHA`) VALUES
(4, 'ADMIN', 'ADMIN ADMIN', 'Alicante', 'dani_sc1993@hotmail.com', '1234ADMIN ADMIN', '$2y$10$h.TXzI56NJX8urbxsU38puFvyf6sbRoEyfTF81Z0mMXDHhzVGUABa', 'userDefault.jpg', 'ROLE_ADMIN', '2019-01-07 16:53:36'),
(5, 'Roberto', 'Sempere Maestre', 'Alicante', 'robert@robert.es', '1234Sempere Maestre', '$2y$10$9AcJVdYntvwzYkoOfTGrHej.RvxZ9eB//0uCF9cUpAGwAd8ge8kNq', 'userDefault.jpg', 'ROLE_USER', '2019-01-07 16:56:06'),
(6, 'Adrian', 'Gimenez Marcos', 'Alicante', 'adri@gmail.es', '1234Gimenez Marcos', '$2y$10$bcE8blE5AXe/84R6F0epxeI1v4.eA2I.4AU4uPsa7Tc58tp1mZD6q', 'userDefault.jpg', 'ROLE_USER', '2019-01-07 16:56:44'),
(7, 'Pedro', 'Martinez', 'Alicante', 'pedro@gmail.es', '1234Martinez', '$2y$10$Kn.pEbt5WwSVpIsUmITD3e5YJ2prBWuxma7EYebPUvHP6anbOeDQG', 'userDefault.jpg', 'ROLE_USER', '2019-01-07 16:57:32');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `COMMENT`
--
ALTER TABLE `COMMENT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_POST_ID` (`ID_POST`),
  ADD KEY `ID_USER` (`ID_USER`);

--
-- Indices de la tabla `INSCRIPCIONES`
--
ALTER TABLE `INSCRIPCIONES`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_ID_USUARIO` (`ID_USUARIO`),
  ADD KEY `FK_ID_EVENTO` (`ID_EVENTO`);

--
-- Indices de la tabla `MESSAGE`
--
ALTER TABLE `MESSAGE`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `POST`
--
ALTER TABLE `POST`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_USER_ID` (`ID_USUARIO`);

--
-- Indices de la tabla `SUBSCRIPCION`
--
ALTER TABLE `SUBSCRIPCION`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `USUARIO`
--
ALTER TABLE `USUARIO`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `COMMENT`
--
ALTER TABLE `COMMENT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `INSCRIPCIONES`
--
ALTER TABLE `INSCRIPCIONES`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `MESSAGE`
--
ALTER TABLE `MESSAGE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `POST`
--
ALTER TABLE `POST`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `SUBSCRIPCION`
--
ALTER TABLE `SUBSCRIPCION`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `USUARIO`
--
ALTER TABLE `USUARIO`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `COMMENT`
--
ALTER TABLE `COMMENT`
  ADD CONSTRAINT `FK_POST_ID` FOREIGN KEY (`ID_POST`) REFERENCES `POST` (`ID`),
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `USUARIO` (`ID`);

--
-- Filtros para la tabla `INSCRIPCIONES`
--
ALTER TABLE `INSCRIPCIONES`
  ADD CONSTRAINT `FK_ID_EVENTO` FOREIGN KEY (`ID_EVENTO`) REFERENCES `POST` (`ID`),
  ADD CONSTRAINT `FK_ID_USUARIO` FOREIGN KEY (`ID_USUARIO`) REFERENCES `USUARIO` (`ID`);

--
-- Filtros para la tabla `POST`
--
ALTER TABLE `POST`
  ADD CONSTRAINT `FK_USER_ID` FOREIGN KEY (`ID_USUARIO`) REFERENCES `USUARIO` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
