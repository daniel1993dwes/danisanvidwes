'use strict';

document.addEventListener('DOMContentLoaded', event =>{
        let buttons = document.querySelectorAll('#reply');
        buttons.forEach(b =>{
            b.addEventListener('click', CrearFormulario)
        })
})

function CrearFormulario(e) {
    e.preventDefault();
    let button = e.currentTarget;
    let parent = button.parentElement.parentElement;
    let inputs = parent.querySelectorAll('input');
    parent.querySelector('#replyComment').innerHTML = '<form class="comment" action="/new/message" method="post" enctype="multipart/form-data">'+
    '<div class="row"><div class="col-sm-8">'+
    '<input type="text" name="idUser" class="hidden" value="'+ inputs[0].value +'"/>'+
    '<input type="text" name="idRecive" class="hidden" value="'+ inputs[1].value +'"/>'+
    '<input type="text" name="idComment" class="hidden" value="'+ inputs[2].value +'"/>'+
    '<div><textarea rows="1" cols="1" name="message" placeholder="Message"></textarea></div><div>'+
    '<input type="submit" class="btn-default" value="Submit"/>'+
    '<a class="btn btn-danger espacio" href="#" role="button" id="hide">Hide</a></div></div></div></form>';
    button.parentElement.style.display = 'none';

    parent.querySelector('#hide').addEventListener('click', event =>{
        event.preventDefault();
        parent.querySelector('#replyComment').innerHTML = '';
        button.parentElement.style.display = '';
    })
}